#include <iostream>

// Define a new enumeration named Color
enum Color
{
    // Here are the enumerators
    // These define all the possible values this type can hold
    // Each enumerator is separated by a comma, not a semicolon
    COLOR_BLACK,
    COLOR_RED,
    COLOR_BLUE,
    COLOR_GREEN,
    COLOR_WHITE,
    COLOR_CYAN,
    COLOR_YELLOW,
    COLOR_MAGENTA, // with nowadays compilers, the trailing comma is allowed, but not needed
}; // however the enum itself must end with a semicolon

enum class Monster
{
    MONSTER_ORC,
    MONSTER_GOBLIN,
    MONSTER_TROLL,
    MONSTER_OGRE,
    MONSTER_SKELETON,
};


int main()
{
    // Define a few variables of enumerated type Color
    Color paint = COLOR_WHITE;
    Color house(COLOR_BLUE);
    Color apple { COLOR_RED };
    Monster Voljin(Monster::MONSTER_TROLL); // class is strong typed and strong scoped, the entries in Monster are no longer treated as integers -> we must convert them explicitly

    return 0;
}