#include <iostream>

class Something {
private:
    static int s_value;
public:
    static int getValue() { return s_value; } // static member function
};

int Something::s_value { 1 }; // initializer

/* 1.   static member functions have no *this pointer, *this always point to the object that the member function is working on
 *      static member functions don't work on an object so the *this pointer is not needed
 *
 * 2.   static member functions can directly access other static members (variables or functions), but NOT non-static members.
 *      this is because non-static members must belong to a class object, and static member functions have no class objects to work with*/

class IDGenerator {
private:
    static int s_nextID;    // here's the declaration for a static member variable
public:
    static int getNextID(); // here's the declaration for a static member function
};

// here's the definition of the static member outside the class. note we can't use the static keyword here
// we'll start generating IDs at 1
int IDGenerator::s_nextID { 1 };

// here't the definition of the static function outside of the class. note we don't use the static keyword here
int IDGenerator::getNextID() { return s_nextID++; }

int main() {
    for (int count { 1 }; count < 5; ++count) {
        std::cout << "The next ID is: " << IDGenerator::getNextID() << '\n';
    }
    return 0;
}