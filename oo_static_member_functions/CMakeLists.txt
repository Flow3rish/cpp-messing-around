cmake_minimum_required(VERSION 3.12)
project(oo_static_member_functions)

set(CMAKE_CXX_STANDARD 14)

add_executable(oo_static_member_functions main.cpp)