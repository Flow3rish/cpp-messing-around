#ifndef CHARS_CONVERSION_H
#define CHARS_CONVERSION_H

char inputChar();
int inputInt();
void convertCharToInt(char);
void convertIntToChar(int);

#endif //CHARS_CONVERSION_H
