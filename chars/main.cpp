#include <iostream>
#include "conversion.h"

/* program se zepta, jestli chceme konvertnout kod na char, nebo char na kod,
 * potom vezme input, ktery prekonvertuje a vytiskne. */

int main()
{
    std::cout << "0 - convert char to code" << std::endl;
    std::cout << "1 - convert code to char" << std::endl;
    std::cout << "your option: ";

    int option;
    std::cin >> option;

    if(option == 0)
    {
        char ch;
        ch = inputChar();
        convertCharToInt(ch);
    }
    if(option == 1)
    {
        int x(inputInt());
        convertIntToChar(x);
    }
    else
        std::cout << "Invalid option." << std::endl;



    return 0;
}