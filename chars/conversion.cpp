#include <iostream>

char inputChar()
{
    std::cout << "input a keyboard character: ";
    char ch;
    std::cin >> ch;
    return ch;
}

int inputInt()
{
    std::cout << "Input an integer: ";
    int x;
    std::cin >> x;
    return x;
}

void convertCharToInt(char ch)
{
   std::cout << ch << " has ASCII code " << static_cast<int>(ch) << std::endl;
}

void convertIntToChar(int x)
{
   std::cout << x << "has coresponds to " << static_cast<char>(x) << " in ASCII." << std::endl;
}


