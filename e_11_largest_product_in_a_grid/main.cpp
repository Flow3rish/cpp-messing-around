#include <iostream>
#include <fstream>
#include <vector>


int main() {
    std::vector<int> list;
    std::ifstream inputFile("/home/archvita/CLionProjects/e_11_largest_product_in_a_grid/grid_raw.txt");

    // checks if file exists
    if (inputFile.good()) {
        int current_number = 0;
        while (inputFile >> current_number) {
            list.push_back(current_number);
        }
        inputFile.close();
    }
    else {
        std::cout << "error\n";
    }

    //for (int i = 0; i < list.size(); ++i) {
    //    if (i % 20 == 0) {
    //        std::cout << '\n';
    //    }
    //    std::cout << list[i] << '\t';
    //}

    int array[20][20];
    for (int i = 0, list_index = 0; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            array[i][j] = list[list_index];
            ++list_index;
        }
    }

    //for (int i = 0; i < 20; ++i) {
    //    for (int j = 0; j < 20; ++j) {
    //        std::cout << array[i][j] << '\t';
    //    }
    //    std::cout << '\n';
    //}
    int product = 1;
    int max = 0;


    // left and right
    for (int i = 0; i < 20; ++i) {
        for (int j = 3; j < 20; ++j) {
            product = 1;
            product = product * (array[i][j - 3] * array[i][j - 2] * array[i][j - 1] * array[i][j]);
            if (product > max) {
                max = product;
            }
        }
    }

    std::cout << max << '\n';

    // up and down
    for (int i = 3; i < 20; ++i) {
        for (int j = 0; j < 20; ++j) {
            product = 1;
            product = product * (array[i - 3][j] * array[i - 2][j] * array[i - 1][j] * array[i][j]);
            if (product > max) {
                max = product;
            }
        }
    }
    std::cout << max << '\n';

    // diagonals
    for (int i = 3; i < 20; ++i) {
        for (int j = 3; j < 20; ++j) {
            product = 1;
            product = product * (array[i - 3][j - 3] * array[i - 2][j - 2] * array[i - 1][j - 1] * array[i][j]);
            if (product > max) {
                max = product;
            }
        }
    }
    std::cout << max << '\n';

    for (int i = 3; i < 20; ++i) {
        for (int j = 3; j < 20; ++j) {
            product = 1;
            product = product * (array[i - 3][j] * array[i - 2][j - 1] * array[i - 1][j - 2] * array[i][j - 3]);
            if (product > max) {
                max = product;
            }
        }
    }
    std::cout << max << '\n';

    return 0;
}