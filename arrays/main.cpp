#include <iostream>

namespace Animals {
    enum Animals {
       ANIMAL_CHICKEN,
       ANIMAL_DOG,
       ANIMAL_CAT,
       ANIMAL_ELEPHANT,
       ANIMAL_DUCK,
       ANIMAL_SNAKE,
       MAX_ANIMALS,
    };
}

int main() {
    int prime[5];
    prime[0] = 2;
    prime[1] = 3;
    prime[2] = 5;
    prime[3] = 7;
    prime[4] = 11; // this is quite painful though

    int prime_2[5] = { 2, 3, 5 ,7 , 11 };
    int nuder_erej[9] = {3, 54, 12, 5}; //initializes only first 4 elements
    int kokot[123] = {}; // initializes all elements to 0 (or whatever zero is in other data types)
    int uniform[4] {2, 3, 4, 5}; // use uniform initialization
    int erej[] {1, 2, 3, 4}; // creates array of length 4

    //quiz time

    int temperature_on_day[365] {};
    int animals_legs[Animals::MAX_ANIMALS] {2, 4, 4, 4, 2, 0};
    std::cout << "Elephant has " << animals_legs[Animals::ANIMAL_ELEPHANT] << " legs.\n";

    std::cout << "the lowes prime number is " << prime[0] << '\n';
    std:: cout << "the sum of first 5 prime numbers is " << prime[0] + prime[1] + prime[2] + prime[3] + prime[4] << '\n';
    return 0;
}