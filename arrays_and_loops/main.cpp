#include <iostream>

int getInput() {
    while (true) {
        int user_input;
        std::cout << "Input a number from 1 to 9: ";
        std::cin >> user_input;
        // if the user entered something invalid
        if (std::cin.fail()) {
            std::cin.clear(); // reset any error flags
            std::cin.ignore(32767, '\n'); // ignore any characters in the input buffer
        } else
            return user_input;
    }
}

int main() {
/*    int array[]{ 4, 6, 7, 3, 8, 2, 1, 9, 5 };
    const int array_length{ sizeof(array) / sizeof(array[0]) };
    for(int count{0}; count < array_length; ++count) {
        std::cout << array[count] << '\t';
    }
    std::cout << '\n';

    // quiz //
    int quiz_array[] { 4, 6, 7, 3, 8, 2, 1, 9, 5 };
    const int max_index{sizeof(quiz_array)/ sizeof(quiz_array[0])};

    int user_input{getInput()};
    for (int index(0); index < max_index; ++index ) {
        std::cout << quiz_array[index] << '\t';
        if(quiz_array[index] == user_input) {
            std::cout << user_input << " has index " << index << '\n';
        }
    }*/

    // quiz3 //
    int scores[] = { 84, 92, 76, 81, 56 };
    const int numStudents = sizeof(scores) / sizeof(scores[0]);

    int maxScore = 0; // keep track of our largest score
    int maxIndex;     // kee track of the index with the largest score

    // now look for a larger score
    for (int student = 0; student < numStudents; ++student) {
        if (scores[student] > maxScore) {
            maxScore = scores[student];
            maxIndex = student;
        }
    }

    std::cout << "The best score was " << maxScore << '\n';

    return 0;
}