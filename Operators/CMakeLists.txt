cmake_minimum_required(VERSION 3.12)
project(Operators)

set(CMAKE_CXX_STANDARD 14)

add_executable(Operators main.cpp)