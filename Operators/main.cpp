#include <iostream>

int main() {
    //literals jsou v podstate konstanty, ktere jsou vlozeny primo do source kodu a evaluuji se primo na sebe, priklady:
    int x = 2; // 2 je literal
    std::cout << 3 + 4 << std:endl; // 3 + 4 je expresion, 3 a 4 jsou literaly
    std::cout << "Hello, world!"; // "Hello, world!" je literal

    return 0;
}
/* Existuje mnoho operatoru, ktere kombinuji operandy, priklad je "+," coz je operator, ktery kombinuje dva operandy (binarni)
 * priklad unarniho operatoru je "-," aplikuje se na jeden operand, napr. -5 */