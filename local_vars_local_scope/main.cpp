#include <iostream>

int getInteger()
{
    std::cout << "Enter an integer: ";
    int x;
    std::cin >> x;
    return x;
}

int main() {
    int a(getInteger());
    int b(getInteger());

    if (b < a)
    {
        std::cout <<"Swapping the values...\n";
        a = a + b;
        b = a - b;
        a = a - b;

    }

    std::cout << "Smaller int: " << a << "\nLarger int : " << b << "\n";

    return 0;
}

