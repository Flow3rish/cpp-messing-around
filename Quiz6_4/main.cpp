#include <iostream>

void printCStyleString(const char *c_string) {
    while (*c_string != '\0') {
        std::cout << *c_string;
        c_string++;
    }
}

int main() {

    printCStyleString("Hello, World");
    return 0;
}