#include <iostream>

// function prototypes //

int sumTo(const int x);

void printEmployee(const Employee &employee);

void minmax(const int x, const int y, int &min_out, int &max_out);

int getIndexOfLargestValue(const int *array, const int array_size);

const int& getElement(const int *array, const int index);


int main() {
    return 0;
}