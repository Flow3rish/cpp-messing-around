#include <iostream>

bool isPrime(unsigned long long n) {
    if (n == 2 || n == 3) {
        return true;
    }
    if (n == 4) {
        return false;
    }

    for (unsigned long long i { 3 }; i <= n/2 + 1; ++i) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    unsigned long long sum = 2;
    for (unsigned long long i = 3; i < 2000000; i += 2) {
        if(isPrime(i)) {
           sum += i;
           std::cout << sum << std::endl;
        }
    }
    std::cout << "FINAL " << sum << '\n';

    return 0;
}