#include <iostream>
#include <cassert>

class Fraction {
private:
    int m_numerator;
    int m_denominator;

public:
//  Fraction() {                                // default constructor
//      m_numerator = 0;
//      m_denominator = 1;
//  }

//  Fraction(int numerator, int denominator = 1) { // constructor with two parameters, one parameter having a default value
//      assert(denominator != 0);
//      m_numerator = numerator;
//      m_denominator = denominator;
//  }       // this constructor will, however, make our previous default constructor redundant

    /* two constructor can coexist peacefully due to function overloading
     * we can define as many constructors as we want, as long as each has a unique signature (number and type of parameters)*/


    // this simplified our class
    Fraction(int numerator = 0, int denominator = 1) {
        assert(denominator != 0);
        m_numerator = numerator;
        m_denominator = denominator;
    }

    int getNumerator() { return m_numerator; }
    int getDenominator() { return m_denominator; }
    double getValue() { return static_cast<double>(m_numerator) / m_denominator;}

    /* if no constructor is provided, compiler will implicitly create it's own like so:
     * Fraction() {
     * }
     * as we can see, nothing is initialized
    */

    /* if our class has a constructor, the implicit one will not be provided */
};

int main() {
    Fraction frac; // since no arguments, calls Fraction() default constructor
    std::cout << frac.getNumerator() << '/' << frac.getDenominator() << '\n';

    int x(5);                           // direct initialization of an integer
    Fraction fiveThirds(5, 3);          // direct initialization of a Fraction, calls Fraction(int, int) constructor

    int y { 6 };                        // uniform initialization of an integer
    Fraction sixSevenths { 6, 7 };      // uniform initialization of a Fraction, calls Fraction(int, int) constructor

    // since we have given the second parameter a default value, the following is legal (and there is no constructor, that would take just one parameter)
    Fraction six(6);

    // !!!COPY INITIALIZATION WORKS, BUT SHOULD BE AVOIDED!!! //
    Fraction seven = Fraction(7);
    Fraction eight = 8;                 // copy initialization of a Fraction, the compiler will try to find a way to convert 7 to a Fraction, which will invoke the Fraction(7, 1) constructor
    return 0;
}