cmake_minimum_required(VERSION 3.12)
project(Quiz1)

set(CMAKE_CXX_STANDARD 14)

add_executable(Quiz1 main.cpp io.cpp io.h)