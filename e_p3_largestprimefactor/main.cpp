#include <iostream>


bool isPrime(unsigned long long n) {
    if (n == 2 || n == 3) {
        return true;
    }
    if (n == 4) {
        return false;
    }

    for (unsigned long long i { 3 }; i <= n/2 + 1; ++i) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

void factors(unsigned long long n) {
    unsigned long long max_prime { 0 };
    for (unsigned long long i { 3 }; i < n/2 + 1; i += 2) {
        // is i a factor of n? //
        if (n % i == 0) {
            std::cout << i << ": prime -\t" << isPrime(i) << "\tequals: " << n/i << std::endl;
            }
        }
    }

//unsigned long long factorsNew(unsigned long long n) {
//    for (unsigned long long i { n/2 + 1 }; i > 0; i -= 2) {
//        // is i a factor of n? //
//        if (n % i == 0) {
//            std::cout << i << std::endl;
//            }
//            if (isPrime(i))
//                return i;
//        }
//        return 2;
//    }

int main() {
    factors(600851475143);
    return 0;
}