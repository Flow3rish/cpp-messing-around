cmake_minimum_required(VERSION 3.12)
project(e_p3_largestprimefactor)

set(CMAKE_CXX_STANDARD 14)

add_executable(e_p3_largestprimefactor main.cpp)