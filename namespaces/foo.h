//
// Created by archvita on 8/29/18.
//

#ifndef NAMESPACES_FOO_H
#define NAMESPACES_FOO_H

namespace Foo
{
    int doSomething(int x, int y) {
        return x + y;
    }
}

namespace benis
{
    namespace in_bagina
    {
        const int g_x = 5;
    }
}

namespace seks = benis::in_bagina; // :DDDDDDDDDDDDDDDDDDDD

#endif //NAMESPACES_FOO_H
