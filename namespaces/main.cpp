#include <iostream>
#include "foo.h"
#include "goo.h"

int main()
{
    std::cout << Foo::doSomething(4, 3) << '\n';
    std::cout << Goo::doSomething(4, 3) << '\n';

    std::cout << benis::in_bagina::g_x << '\n'; //
    std::cout << seks::g_x << '\n';             // same thing

    // we should avoid nesting namespaces if possible

    return 0;
}