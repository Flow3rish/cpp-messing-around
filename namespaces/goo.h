//
// Created by archvita on 8/29/18.
//

#ifndef NAMESPACES_GOO_H
#define NAMESPACES_GOO_H

namespace Goo
{
    int doSomething(int x, int y) {
        return x - y;
    }
}

#endif //NAMESPACES_GOO_H
