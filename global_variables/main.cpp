#include <iostream>

// try to avoid non const global variables

static double g_global_variable(1.2); //this variable is accessible anywhere, but only in this file
extern double g_another_variable(2.0); // this variable is accessible from other files (extern is default option, here it's redundant), must be uninitialized when forward declaring

void doStaticCodeMate()
{
    std::cout << "Incrementing with static variable: ";
    static int s_number(1);
    s_number++;
    std::cout << s_number << '\n';
}

void doCodeMate()
{
    std::cout << "Incrementing with non-static variable: ";
    int number(1);
    number++;
    std::cout << number << '\n';
}

int main()
{
    // do code
    {
       static int s_x(4); // static variable is created here and is accessible here
       char y(8);
    }
    // s_x is not destroyed here, but is no longer acessible
    // y is destroyed here

    //let's apply this//
    doCodeMate();
    doCodeMate();

    doStaticCodeMate();
    doStaticCodeMate();

    //BRO PRO TIP: static variables are great for making unique IDs//

    return 0;
}