#include <iostream>

int factorial(int n) {
    if (n > 1) {
        return n * factorial(n - 1);
    }
    else
        return 1;
}

int sumDigits(int n) {
    if (n < 10) {
        return n;
    } else
        return sumDigits(n / 10) + n % 10; // thanks to the integer implementation in computers, this works magic
}

int getIntegerDec() {
    std::cout << "input an integer: ";
    int x;
    std::cin >> x;

    return x;
}

int decToBin(unsigned int n) {

    if (1 < n) {
        decToBin(n/2);
        std::cout << n % 2;
        return n;
    }
    else {
        std::cout << n;
        return n;
    }

}

int main() {
    //std::cout << factorial(6) << '\n';
    int x = getIntegerDec();
    decToBin(x);
    return 0;
}