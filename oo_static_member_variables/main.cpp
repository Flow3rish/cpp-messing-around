#include <iostream>
#include <array>

class Something {
public:
    int m_value { 1 };
    static int s_value; // declares the static member variable
    static const int s_const_value { 4 }; // a static const int can be declared and initialized directly
    static constexpr double s_constexpr_value { 2.2 }; // ok even for compile-time constants
    static constexpr std::array<int, 3> s_array { 1, 2, 3 }; // this even works for classes that support constexpr initialization

    // a great example to why use static member variables is to assign unique ID to every instance od the class exempli gratima:
private:
    static int s_idGenerator;
    int m_id;

public:
    Something() { m_id = s_idGenerator++; } // grab the next value form the id generator

    int getID() const { return m_id; }
};

// note that we're defining and initializing s_idGenerator even though it is declared as pirvate above
// this is okay since the definition isn't subject to access controls
int Something::s_idGenerator = 1; // start our ID generator with value 1

int Something::s_value { 1 }; // defines the static member variable - initializes it to 1 (if not explicitly stated otherwise, variable initializes to 0)


int main() {
    /*Something first;
    Something second;

    first.m_value = 2;
    first.s_value = 2;

    std::cout << first.m_value << '\n';
    std::cout << second.m_value << '\n';

    std::cout << first.s_value << '\n';
    std::cout << second.s_value << '\n';    // because s_value is a static member variable, it's shared between all objects of the class
                                            // consequently, first s_value is the same as second s_value, this program shows, that the value we set using fist can be accessed using second


    */

    Something::s_value = 2;                 // all instantiation is commented out, so no objects are instantiated, yet the variable can be accessed, it belongs to the class itself!
    std::cout << Something::s_value << '\n';

    Something first;
    Something second;
    Something third;

    std::cout << first.getID()  << '\n';
    std::cout << second.getID() << '\n';
    std::cout << third.getID()  << '\n';

    return 0;
}