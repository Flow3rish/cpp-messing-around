cmake_minimum_required(VERSION 3.12)
project(foreach_loop)

set(CMAKE_CXX_STANDARD 14)

add_executable(foreach_loop main.cpp)