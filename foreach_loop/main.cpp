#include <iostream>
#include <string>

void isInArray(std::string &names, std::string my_name) {
    for (const auto &name : names) {
        if (name == my_name)
            std::cout << "your name is in the list.";
        else
            std::cout << "your name isn't in the list.";
    }
}

int main() {

    const std::string names[8] = {
            "Alex",
            "Betty",
            "Caroline",
            "Dave",
            "Emily",
            "Fred",
            "Greg",
            "Holly"
    };
    std::string &refNames = names[8];



    std::cout << "Enter a name:\n";
    std::string name;
    std::cin >> name;


    return 0;
}