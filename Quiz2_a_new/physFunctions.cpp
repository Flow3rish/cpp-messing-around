//
// Created by archvita on 8/27/18.
//
#include <iostream>
#include "myConstants.h"

double getHeight()
{
    double init_height;
    std::cout << "Input height: ";
    std::cin >> init_height;
    return init_height;
}

void calculateHeightAtTime(double init_height, int time)
{
    double height(init_height - constants::gravitational_constant * time * time * 0.5);
    if (height < 0)
        std::cout << "the ball is on the ground at " << time << " seconds.\n";
    else
        std::cout << "ball\'s height is " << height << " at " << time << " seconds.\n";
}

