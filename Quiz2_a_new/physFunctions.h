//
// Created by archvita on 8/27/18.
//

#ifndef QUIZ2_A_NEW_PHYSFUNCTIONS_H
#define QUIZ2_A_NEW_PHYSFUNCTIONS_H

double getHeight();
double calculateHeightAtTime(double, int);

#endif //QUIZ2_A_NEW_PHYSFUNCTIONS_H
