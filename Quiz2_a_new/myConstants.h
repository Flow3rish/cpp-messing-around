//
// Created by archvita on 8/27/18.
//

#ifndef QUIZ2_A_NEW_MYCONSTANTS_H
#define QUIZ2_A_NEW_MYCONSTANTS_H

namespace constants
{
    constexpr double gravitational_constant(9.8);
}

#endif //QUIZ2_A_NEW_MYCONSTANTS_H
