cmake_minimum_required(VERSION 3.12)
project(e_8_Largest_product_in_a_series)

set(CMAKE_CXX_STANDARD 14)

add_executable(e_8_Largest_product_in_a_series main.cpp)