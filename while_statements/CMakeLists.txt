cmake_minimum_required(VERSION 3.12)
project(while_statements)

set(CMAKE_CXX_STANDARD 14)

add_executable(while_statements main.cpp)