#include <iostream>

auto main() -> int {
    int count = 1;
    while (count <= 100) {
        if (count < 10)
            std::cout << "0" << count << " ";
        else
            std::cout << count << " ";

        if(count % 10 == 0)
            std::cout << '\n';

        ++count;
    }
    return 0;
}