#include <iostream>

int main() {
    int w;
    w = 6;
    int x;
    x = 5;

    int *ptr; // defines a pointer
    ptr = &x; // initializes pointer with address of x



    std::cout << ptr << '\n'; // prints address stored in pointer
    std::cout << *ptr << '\n'; // prints dereferenced pointer (asks pointer to show the value it point to)

    const int *cptr;
    cptr = &x; // const pointer is initialized and then serves as read-only, values can not be passed through it
    std::cout << *cptr << '\n';
    cptr = &w; // const pointer can, however can change the pointing direction
    std::cout << *cptr << '\n';

    int y;
    y = 8;
    int &ref = y; // initializes a reference, reference must be initialized from the start
    ref = 8; // passes 8 to the reference, this changes value stored in 8

    std::cout << ref << '\n';

    const int &cref = y; // this reference is read-only, it's hard wired tunnel to the value (non const referenced value can be changed though)





    return 0;
}