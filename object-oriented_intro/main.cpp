#include <iostream>

struct DateStruct {
    int year;
    int month;
    int day;
};

void print(DateStruct &date) {
    std::cout << date.year << '/' << date.month << '/' << date.year << '\n';
}

class DateClass {
public:
    int m_year;
    int m_month;
    int m_day;

    void print() {
        std::cout << m_year << '/' << m_month << '/' << m_day << '\n';
    }
};


// Quiz time //
class Pair {
public:
    int number_1;
    int number_2;

    void setIntegers() {
        std::cout << "Input the first number: ";
        std::cin >> number_1;
        std::cout << "Input the second number: ";
        std::cin >> number_2;
    }

    void printIntegers() {
        std::cout << "number_1: " << number_1 << '\n';
        std::cout << "number_2; " << number_2 << '\n';
    }
};



int main() {

    // struct manipulation //
    DateStruct today_struct_instance {2020, 10, 14}; // struct uniform initialization
    today_struct_instance.day = 16;                     // struct member variable assignment
    print(today_struct_instance);                       // calling function with struct as a parameter

    // class manipulation //

    DateClass today_class_instance {2020, 10, 14};      // class uniform initialization - object instantiation
    today_class_instance.m_day = 16;                    // class member variable assignment
    today_class_instance.print();                       // class member function call to previously instantiated object

    /* unlike in C, in C++ structs are also able to hold functions, but they are unrecommended to use, because of memory management */


    // Quiz time //
    Pair pair_1;
    pair_1.setIntegers();
    pair_1.printIntegers();





    return 0;

}