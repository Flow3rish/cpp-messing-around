#include <iostream>

int main() {
    int array[3][5]
    {
            { 1, 2, 3, 4, 5 },
            { 44, 24, 34, 1, 0 },
            { 10, 9, 8, 7, 6 }
    };
    int array_2[2][4] { 0 };


    int numRows{3};
    int numCols{5};
    int array_3[numRows][numCols] {
            { 1, 2, 3, 4, 5 },
            { 44, 24, 34, 1, 0 },
            { 10, 9, 8, 7, 6 }
    };
    for (int row {0}; row < numRows; ++row) {
        for (int col{0}; col < numCols; ++col) {
            std::cout << array_3[row][col] << '\t';
            if ((col + 1) % numCols == 0)
                std::cout << '\n';
        }
    }

    return 0;
}