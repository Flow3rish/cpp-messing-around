#include <iostream>



void passByVal(int x) {
    x = 5;
}

void passByRef(int &ref) {
    ref = 5;
}

void passByPtr(int * ptr) {
    *ptr = 8;
}



int main() {
    int value = 3;
    int &ref = value;
    std::cout << ref << '\n'; // prints 3

    int x = 0;
    std::cout << x << '\n';
    passByVal(x);           // passes only a copy of x
    std::cout << x << '\n';
    passByRef(x);           // passes x itself (efficient for larger arguments)
    std::cout << x << '\n';
    int *ptrx;              //
    ptrx = & x;             // we must first create a pointer to pass
    passByPtr(ptrx);
    std::cout << x << '\n';

    struct Hovno {
        bool smrdi;
        int velikost;
    };

    Hovno hovno;

    int &reference = hovno.velikost;

    struct Person {
        int age;
        double weight;
    };

    Person person;
    Person &refer = person;
    refer.age = 5;

    Person *ptr = &person;
    (*ptr).age = 5;
    ptr->age = 5; // better with pointer






    return 0;
}