#include <iostream>
#include <utility>

int main() {
    const int length{5};
    int array[length] { 1, 2, 3, 4, 5 };
    int max_index = sizeof(array) / sizeof(array[0]);
    bool did_sort{false};
    for (int trim{1}; trim <= max_index - 1; ++trim) {
        // puts the largest number to the end of the array //
        for (int index{0}; index < (max_index - trim); ++index) {
            if (array[index] > array[index + 1]) {
                std::swap(array[index], array[index + 1]);
                did_sort = true;
            }
        }
        if (!did_sort) {
            std:: cout << "already sorted\n";
            break;
        }
    }


    for (int i{0}; i < length; ++i) {
        std::cout << array[i] << '\t';
    }

    return 0;
}