#include <iostream>
#include "Date.h"




// Date constructor
Date::Date(int year, int month, int day) {
    setDate(year, month, day);
}

// Date member function
void Date::setDate(int year, int month, int day) {
    m_month = month;
    m_day = day;
    m_year = year;
}

// defining member functions outside classes
class Calc {
private:
    int m_value { 0 };

public:
    Calc(int value = 0 );

    Calc& add(int value);
    Calc& sub(int value);
    Calc& mult(int value);

    int getValue() { return m_value; }
};

Calc::Calc(int value): m_value(value) {
}

Calc& Calc::add(int value) {
    m_value += value;
    return *this;
}

Calc& Calc::sub(int value) {
    m_value -+ value;
    return *this;
}

Calc& Calc::mult(int value) {
    m_value *= value;
    return *this;
}




int main() {



    return 0;
}