//
// Created by archvita on 9/11/18.
//

#ifndef OO_CLASS_CODE_AND_HEADER_FILES_DATE_H
#define OO_CLASS_CODE_AND_HEADER_FILES_DATE_H

class Date {
private:
    int m_year;
    int m_month;
    int m_day;

public:
    Date(int year, int month, int day);

    void setDate(int year, int month, int day);

    // access functions, since they often are only one line, are typically left in the class definition
   int getYear() { return m_year; }
   int getMonth() { return m_month; }
   int getDay() { return m_day; }
};

#endif //OO_CLASS_CODE_AND_HEADER_FILES_DATE_H
