cmake_minimum_required(VERSION 3.12)
project(array_sort)

set(CMAKE_CXX_STANDARD 14)

add_executable(array_sort main.cpp)