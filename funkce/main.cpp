#include <iostream>

// tady definujeme funkci doPrint()
// void na zacatku znamena, ze funkce nic nevraci
void doPrint() // tady se definuje volana funkce "callee"
{
    std::cout << "In doPrint()" << std::endl;
    // funkce nic nevraci, takze neni treba return statement
}

// int na zacatku znamena, ze funkce vrati integer callerovi
int return5()
{
    // tato funkce vraci integer, takze potrebujeme return statement
    return 5; // vratime integer hodnoty 5 callerovi
}

int getValueFromUser()
{
    std::cout << "Enter an integer: "; // vytiskne prompt k userovi
    int a; // alokuje misto v pameti pro promennou
    std::cin >> a; // vezme input od usera a ulozi do alokovane promenne
    return a; // vrati promennou callerovi
}

// tady se definuje hlavni funkce main, je to zacatek kazdeho programu
int main() {
    //cast s funkci doPrint()
    std::cout << "Starting main()" << std::endl;
    doPrint(); // prerusi se hlavni funkce "main()" a zavola se funkce doPrint(), funkce "main()" je caller (volajici)

    //cast s funkci return5()
    std::cout << return5() << std::endl; // vytiskne 5
    std::cout << return5() + 2 << std::endl; // vytiskne 7

    return5(); // hodnota 5 je vracena do mainu, ale je ignorovana, protoze main() s ni nic nedela

    std::cout << "Ending main()" << std::endl;

    //secte dve cisla od usera a vytiskne vysledek
    int x = getValueFromUser(); //zavolame funkci poprve a vracenou hodnotu ulozime do promenne x
    int y = getValueFromUser(); //zavolame funkci podruhe a vracenou hodnotu ulozime do promenne y
    std::cout << "Soucet je " << x + y << std::endl;

    return 0; // proto je main() definovana jako int
}

/*funkce muzou volat jine funkce, ale !!!definovat funkci uvnitr jine funkce se nesmi!!!*/