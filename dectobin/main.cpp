#include <iostream>
#include "convertFunctions.h"

int main()
{
    const unsigned char option0 = 1 << 0;
    const unsigned char option1 = 1 << 1;
    const unsigned char option2 = 1 << 2;
    const unsigned char option3 = 1 << 3;
    const unsigned char option4 = 1 << 4;
    const unsigned char option5 = 1 << 5;
    const unsigned char option6 = 1 << 6;
    const unsigned char option7 = 1 << 7;

    unsigned int my_flags(0b00000000);

    my_flags |= (option6 | option1 | option5);

    convertDecToBin(my_flags);
    return 0;
}