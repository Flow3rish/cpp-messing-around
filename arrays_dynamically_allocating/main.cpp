#include <iostream>
#include <string>
typedef std::string string;

int nameQuantity() {
    std::cout << "How many names are you going to input?\n";
    int length;
    std::cin >> length;
    return length;
}

string nameGet() {
    std::cout << "Enter a name: ";
    string name;
    std::cin >> name;
    return name;

}

void namesSort(string *array, int length) {
    // sorts names
    int max_index = length;
    bool did_sort{false};
    for (int trim{1}; trim <= max_index - 1; ++trim) {
        // puts the largest number to the end of the array //
        for (int index{0}; index < (max_index - trim); ++index) {
            if (array[index] > array[index + 1]) {
                std::swap(array[index], array[index + 1]);
                did_sort = true;
            }
        }
        if (!did_sort) {
            std:: cout << "already sorted\n";
            break;
        }
    }

    for (int i{0}; i < max_index; ++i) {
        std::cout << array[i] << '\t';
    }
}

void doSomething(int *array) {
    int max_index = sizeof(array) / sizeof(array[0]);
    for (int i = 0; i < max_index; i ++) {
        std::cout << array[i] << '\t';
    }
}



int main() {
    int length = nameQuantity();
    auto *names = new string[length];
    for (int i = 0; i < length; ++i) {
        std::cout << i + 1 << ".\t";
        string name = nameGet();
        std::cout << '\n';
        names[i] = name;
    }


    namesSort(names, length);
    delete[] names;
    names = nullptr;

    int *dynamicArray = new int[4];
    *dynamicArray = {1, 2, 3, 4};


    return 0;
}
