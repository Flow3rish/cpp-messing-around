#include <iostream>

int foo() {
    return 5;
}

int goo() {
    return 6;
}

int doSomething(int x) {
    return x;
}

void takeFunctionasArgument(int x, int y, int(*function)(int, int)) {
    int result = x + y + function(x, y);
    std::cout << result << '\n';
}

int add(int x, int y){
    return x + y;
}

int multiply(int x, int y){
    return x * y;
}

int main() {
    int (*fcnPtr)() = foo; // fcnPtr points to function foo
    fcnPtr = goo; // fcnPtr now points to function goo


    // DON'T DO THIS!!! //
    // fcnPtr = goo(); ---> this would try to assign the return value of goo to fcnPtr


    // calling a function using a function pointer //
    int (*f_pointer)(int) = doSomething; // assign f_pointer to function doSomething
    (*f_pointer)(5); // call function doSomething(5) through f_pointer
    f_pointer(5); // implicit dereference, looks like a normal function call (normal function names are pointers to functions anyway!)


    // using a callback function -> function, that is an argument in another function
    int x = 4;
    int y = 5;
    takeFunctionasArgument(x, y, add);
    takeFunctionasArgument(x, y, multiply);

    return 0;
}