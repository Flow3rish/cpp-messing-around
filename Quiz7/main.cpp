#include <iostream>
#include <random>
#include <vector>

auto getRandomNumber(int min_r, int max_r) -> int {
   	std::random_device rd;
	std::mt19937 mersenne(rd());
	std::uniform_int_distribution<> random_number(min_r, max_r);
	return random_number(mersenne);
}

auto fillSortedVector(std::vector<int> &my_vector, int length, int from, int to) -> void {
   for (int i = 0; i < length; ++i) {
       my_vector[i] = getRandomNumber(from, to);
       int temp = from;
       from = to;
       to = to + (to - temp);
   }

   for (int i = 0; i < length; ++i) {
       std::cout << my_vector[i] << ' ';

       if (i > 0 && i % 10 == 0) {
           std::cout << '\n';
       }
   }
   std::cout << '\n';

}

auto getInteger() -> int {
    std::cout << "input an integer: ";
    int x;
    std::cin >> x;
    return x;
}

auto binarySearch(std::vector<int> sorted_array, const int searched_value, int min_i, int max_i) -> int {
    // takes the integer and takes compares it with the element that's in the middle of the array
    // handles separately the case, when the searched value is greater than the maximal value in the array
    if (searched_value > sorted_array[max_i] || searched_value < sorted_array[min_i]) {
        return -1;
    }
    else {
        for (int searched_index = (min_i + (max_i - min_i) / 2); min_i <= max_i; searched_index = (min_i + (max_i - min_i) / 2)) {

            // for testing
            // std::cout << "searched index set to: " << searched_index << '\n';

            if (searched_value < sorted_array[searched_index]) {
                max_i = searched_index - 1;                                                                             // sets new boundaries
            } else if (searched_value > sorted_array[searched_index]) {
                min_i = searched_index + 1;                                                                             // sets new boundaries
            } else if (searched_value == sorted_array[searched_index]) {
                return searched_index;
            }
        }
        return -1;
    }
}

int main() {
    // binary search //
    /* look at the center element of the array (if the array has an even number of elements, round down
     * if the center element is greater than the target element, discard the top half of the array (or recurse on the bottom half)
     * if the center element is less than the target element, discard the bottom half of the array (or recurse on the top half)
     * if the center element equals the target element, return the index of the center element
     * if you discard the entire array without finding the target element, return a sentinel, that represents "not found" (in this case, we'll use -1, since it's an invalid array index)*/


    int length;
    std::vector<int> my_vector;

    std::cout << "array length -> ";
    length = getInteger();
    my_vector.resize(static_cast<unsigned long>(length));

    fillSortedVector(my_vector, length, 0, 10);

    std::cout << "search number -> ";
    int search = getInteger();
    int found = binarySearch(my_vector, search, 0, length - 1);
    if (found >= 0) {
        std::cout << "value " << search << " was found on index " << found << '\n';
    }
    else {
        std::cout << "value " << search << " isn't present in the array\n";
    }

    return 0;
}