#include <iostream>

// FRIEND FUNCTIONS //

class Value {
private:
    int m_value;
public:
    Value(int value) { m_value = value; }
    friend bool isEqual(const Value &value1, const Value &value2);
};

bool isEqual(const Value &value1, const Value &value2) {
    return (value1.m_value == value2.m_value);
}

class Humidity; // class prototype to tell the compiler that we are going to define this class in the futuru, wihtout this line, the compiler would tell us it doesn't know
                // what a Humidity is when parsing the prototype function printWeather() inside the Temperature class

class Temperature {
private:
    int m_temp;
public:
    Temperature(int temp = 0) { m_temp = temp;}

    friend void printWeather(const Temperature &temperature, const Humidity &humidity);
};

class Humidity {
private:
    int m_humidity;
public:
    Humidity(int humidity = 0) { m_humidity = humidity; }

    friend void printWeather(const Temperature &temperature, const Humidity &humidity);
};

void printWeather(const Temperature &temperature, const Humidity &humidity) {
    std::cout << "The temperature is " << temperature.m_temp <<
        " and the humidity is " << humidity.m_humidity << '\n';
}

// FRIEND CLASSES //

class Storage {
private:
    int m_nValue;
    double m_dValue;
public:
    Storage(int nValue, double dValue) {
        m_nValue = nValue;
        m_dValue = dValue;
    }

    // make the Display class a friend of Storage
    friend class Display;
};

class Display {
private:
    bool m_displayIntFirst;
public:
    Display(bool displayIntFirst) { m_displayIntFirst = displayIntFirst; }

    void displayItem(Storage &storage) {
        if (m_displayIntFirst) {
            std::cout << storage.m_nValue << ' ' << storage.m_dValue << '\n';
        } else {
            std::cout << storage.m_dValue << ' ' << storage.m_nValue << '\n';
        }
    }
};

int main() {

    // friend functions //
    Humidity hum(10);
    Temperature temp(12);

    printWeather(temp,hum);

    // friend classes //
    Storage storage(5, 6.7);
    Display display(false);

    display.displayItem(storage);


    return 0;
}