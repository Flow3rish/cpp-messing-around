#include <iostream>

auto getDouble() -> double {
    while (true) { // loops until valid
        std::cout << "Enter a double value: ";
        double x;
        std::cin >> x;


        if (std::cin.fail()) {              // if previous extraction has failed
            std::cin.clear();               // put back in 'normal' operation mode
            std::cin.ignore(32767, '\n');   // remove the bad input from buffer
            std::cout << "Invalid input\n";
        }
        else                                // extraction succeeds
            return x;
    }
}

int main() {
    getDouble();

    return 0;
}