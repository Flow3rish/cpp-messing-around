#include <iostream>
#include <cassert>

class Stack {
    static const int m_max_length = 10;
    int m_array[m_max_length] { 0 };
    int m_current_length;

public:
    void reset() {
        for (int i = 0; i < m_current_length; ++i) {
            m_array[i] = 0;
        }
        m_current_length = 0;
    }

    bool push(int pushed_element) {
        if (m_current_length < m_max_length) {
            m_array[m_current_length] = pushed_element;
            ++m_current_length;
            return true;
        }
        else {
            return false;
        }
    }

    int pop() {

        assert(m_current_length != 0);
        int popped = m_array[m_current_length];
        m_array[m_current_length] = 0;
        --m_current_length;
        return popped;
    }

    void print() {
        std::cout << "{ ";
        for (int i = 0; i < m_current_length; ++i) {
            std::cout << m_array[i] << ' ';
        }
        std::cout << " }";
    }

};

int main() {
	Stack stack;
	stack.reset();

	stack.print(); // good till here

	stack.push(5);
	stack.push(3);
	stack.push(8);
	stack.print();

	stack.pop();
	stack.print();

	stack.pop();
	stack.pop();

	stack.print();
    return 0;
}