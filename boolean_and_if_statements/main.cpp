#include <iostream>

bool isEqual(int x, int y)                                  // funkce, ktera vraci true/false
{
    return (x == y);                                        // operator == kontroluje rovnost
}

int takeIntegerFromUser()
{
    int x;
    std::cout << "Zadej cislo: ";
    std::cin >> x;
    return x;
}

int main()
{
    std::cout << true << std::endl;
    std::cout << !true << std::endl; // not operator "!" z tohoto udela false

    bool b(false);
    std::cout << b << std::endl;
    std::cout << !b << std::endl;

    std::cout << std::boolalpha;     // prints bools as true or false

    std::cout << true << std::endl;
    std::cout << false << std::endl;

    std::cout << std::noboolalpha;  // turns printing back to 0 and 1

    std::cout << true << std::endl;
    std::cout << false << std::endl;

    // if statement
    int cislo;
    if (cislo == 0)                                             // kontroluje, jestli je podminka pravdiva
        std::cout << "The condition is true" << std::endl;  // jeden statement bez {}
    else
    {
        std::cout << "The condition is false" << std::endl; // vice statementu s {}
        std::cout << "And that's all, folks!" << std::endl;
    }

    int x(takeIntegerFromUser());
    int y(takeIntegerFromUser());

    if(isEqual(x, y))
        std::cout << "Integery jsou si rovny" << std::endl;
    else
        std::cout << "Integery si nejsou rovny" << std::endl;



    return 0;
}


