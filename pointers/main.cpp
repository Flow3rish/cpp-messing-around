#include <iostream>

int main() {

    int x = 7;
    int *pointer = &x;
    std::cout << *pointer << '\n';
    *pointer = 6;
    std::cout << x << '\n';

    // pointer arithmetic //

    int value{7};
    int *ptr{&value};

    using std::cout;
    cout << ptr << '\n';
    cout << ptr + 1 << '\n';
    cout << ptr + 2 << '\n';
    cout << ptr + 3 << '\n';

    int array[] = { 9, 7, 5, 3, 1 };

    std::cout << "Element 0 is at address: " << &array[0] << '\n';
    std::cout << "Element 1 is at address: " << &array[1] << '\n';
    std::cout << "Element 2 is at address: " << &array[2] << '\n';
    std::cout << "Element 3 is at address: " << &array[3] << '\n';

    return 0;

    return 0;
}