#include <iostream>
#include <string>   // string is not a core data type

int main()
{
    {
        std::string name("Vita");   // string behaves as any kind of variablea
    }

    {
        std::cout << "Pick 1 or 2: ";
        int choice(0);
        std::cin >> choice;

        std::cin.ignore(32767, '\n'); // ignore up to 32767 characters until a \n is removed
                                      // when combining inputs cin and getline, this should be added

        std::cout << "Enter your full name: ";
        std::string name;
        std::getline(std::cin, name);   // must be used instead of just std::cin >> name;
                                        // std::cin >> name; breaks when whitespace is put in
        std::cout << "Your name is " << name << " and you picked " << choice << "... btw, this string is " << name.length() << " characters long" << '\n';

        std::string a("11");
        std::string b("22");

        std::cout << a + b << '\n';     // strings concatenate
        a += " hodin";
        std::cout << a << '\n';
    }


    //quiz time!//

    std::cout << "Your name: ";
    std::string name;
    std::getline(std::cin, name);

    std::cout << "Your age: ";
    int age;
    std::cin >> age;
    std::cin.ignore(32767, '\n');
    //double for_each_letter = static_cast<double>(age) / name.length();
    std::cout << "You've lived " << static_cast<double>(age) / (name.length() - 1) << " years for each letter in your name.\n";



    return 0;
}