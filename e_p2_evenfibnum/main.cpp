#include <iostream>

bool isEven(long n) {
    if (n % 2 == 0)
        return true;
    return false;
}



int main() {

    unsigned long a { 0 };
    unsigned long b { 1 };
    unsigned long next_term { 0 };
    unsigned long sum { 0 };

    for (unsigned long i {0}; next_term <= 4000000; ++i) {
        next_term = a + b;
        a = b;
        b = next_term;
        if (isEven(next_term)) {
            sum += next_term;
        }
    }
    std::cout << sum << std::endl;

    return 0;
}