#include <iostream>

/* 1)
 * a) true
 * b) false
 * c) true
 * d) 4
 *
 * 2)
 * a) 1
 * b) 4
 *
 * 3)
 * a) 13
 * b) 46
 *
 * 4)
 * a) 1111
 * b) 110101 */

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}