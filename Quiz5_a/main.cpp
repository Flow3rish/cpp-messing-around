#include <iostream>
#include <random>

auto getGuess(int countdown) -> int {
   while (true) {
       std::cout << "Remaining guesses (" << countdown << ") Guess: ";
       int guess;
       std::cin >> guess;

       if (std::cin.fail()) { // has extraction failed?
           // yep, so let's handle the failure
           std::cout << "Invalid input, try again.\n";
           std::cin.clear(); // put us back in 'normal' operation mode
           std::cin.ignore(32767, '\n'); // and remove the bad input
       } else
           return guess;
   }
}

auto guessNumber(int generated_number) -> void {
    for (int countdown{7}; countdown >= 0; --countdown) {
        if (countdown == 0) {
            std::cout << "you have LOST, " << generated_number << " was right.\n";
            break;
        }
        int your_guess{getGuess(countdown)};

        if (your_guess == generated_number) {
            std::cout << "you have WON, " << your_guess << " is right.\n";
            break;
        }
        else if (your_guess < generated_number) {
            std::cout << "LO\n";
        }
        else {
            std::cout << "HI\n";
        }
    }
}

auto generateNumber() -> int {
    std::random_device seed;
    std::mt19937 mersenne(seed());
    std::uniform_int_distribution<> generated_number(1, 100);
    return generated_number(mersenne);
}


int main() {
    char option;
        do {
        std::cout << "I\'m thinking of a number between 1 and 100. Guess it now.\n";
        int generated_number{generateNumber()};
        guessNumber(generated_number);

        while (true) {
            std::cout << "Would you like to play again? (y/n): ";
            std::cin >> option;
            if (option == 'y' || option == 'n' ) {
                break;

            }
            else {
                //let's handle the failure
                std::cout << "Invalid input, try again.\n";
                std::cin.clear(); // put us back in 'normal' operation mode
                std::cin.ignore(32767, '\n'); // and remove the bad input
            }
        }

    }
    while (option == 'y');

    return 0;
}