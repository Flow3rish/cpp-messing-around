#include <iostream>

class DateClass {
    int m_month;
    int m_day;
    int m_year;

public:
    void setDate(int month, int day, int year) {
        // setDate() can access the private members of the class because it is a member of the class itself
        m_month = month;
        m_day = day;
        m_year = year;
    }

    void print() {
        std::cout << m_month << '/' << m_day << '/' << m_year << '\n';
    }

    void copyFrom(const DateClass &d) {
        // note that we can access the private members of d directly
        m_month = d.m_month;
        m_day = d.m_day;
        m_year = d.m_year;
    }
};

// Quiz time //
class Point3d {
    double m_x;
    double m_y;
    double m_z;

public:
    void setValues(double x, double y, double z) {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    void print() {
        std::cout << "point: " << "(" << m_x << ", " << m_y << ", " << m_z << ')' << '\n';
    }

    void isEqual(Point3d point_2) {
        if (m_x == point_2.m_x &&
            m_y == point_2.m_y &&
            m_z == point_2.m_z) {

            std::cout << "point1 and point2 are equal\n";
        }
        else {
            std::cout << "point1 and point2 are not equal\n";
        }
    }
};

int main() {
    DateClass date;
    date.setDate(10, 14, 2020); // okay, because setDate() is public
    date.print(); // okay, because print() is public

    DateClass copy;
    copy.copyFrom(date);
    copy.print();

    // Quiz time //
    Point3d point_1;
    point_1.setValues(3.5, 6.6, 10.0);
    point_1.print();

    Point3d point_2;
    point_2.setValues(3.5, 6.6, 10.0);

    point_1.isEqual(point_2);
    return 0;
}