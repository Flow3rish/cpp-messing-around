#include <iostream>

typedef int error_t;
using error_t = int;

int main() {
    error_t printData();
    int x{2};
    std::cout << x;
    return 0;
}