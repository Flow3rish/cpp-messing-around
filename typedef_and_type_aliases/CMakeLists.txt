cmake_minimum_required(VERSION 3.12)
project(typedef_and_type_aliases)

set(CMAKE_CXX_STANDARD 14)

add_executable(typedef_and_type_aliases main.cpp)