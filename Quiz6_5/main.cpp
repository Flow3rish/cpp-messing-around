#include <iostream>
#include <array>
#include <random>

enum CardRank {
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE,
    MAX_RANKS
};

enum CardSuit {
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES,
    MAX_SUITS
};

struct Card {
    CardRank rank;
    CardSuit suit;
};

struct Player {
    int value;
    int card_counter;
};

auto printCard(const Card &card) -> void {
    std::cout << "Card dealt: ";
    switch (card.rank) {
        case TWO:       std::cout << '2'; break;
        case THREE:     std::cout << '3'; break;
        case FOUR:      std::cout << '4'; break;
        case FIVE:      std::cout << '5'; break;
        case SIX:       std::cout << '6'; break;
        case SEVEN:       std::cout << '7'; break;
        case EIGHT:       std::cout << '8'; break;
        case NINE:       std::cout << '9'; break;
        case TEN:       std::cout << "10"; break;
        case JACK:       std::cout << "J"; break;
        case QUEEN:       std::cout << 'Q'; break;
        case KING:       std::cout << 'K'; break;
        case ACE:       std::cout << 'A'; break;
        default: std::cout << "ERROR(rank)" ; break;
    }

    switch (card.suit) {
        case CLUBS: std::cout << "C\n"; break;
        case DIAMONDS:  std::cout << "D\n"; break;
        case HEARTS:    std::cout << "H\n"; break;
        case SPADES:    std::cout << "S\n"; break;
        default: std::cout << "ERROR(suit)"; break;
    }
}

auto printDeck(const std::array<Card, 52> &deck_of_cards) -> void {
    for (const auto &card : deck_of_cards) {
        printCard(card);
        std::cout << ' ';
    }

    std::cout << '\n';
}

auto getCardValue(Card card) -> int {
    switch (card.rank) {
        case TWO:      return 2 ;
        case THREE:    return 3 ;
        case FOUR:     return 4 ;
        case FIVE:     return 5 ;
        case SIX:      return 6 ;
        case SEVEN:    return 7 ;
        case EIGHT:    return 8 ;
        case NINE:     return 9 ;
        case TEN:      return 10 ;
        case JACK:     return 10 ;
        case QUEEN:    return 10 ;
        case KING:     return 10 ;
        case ACE:      return 11 ;
        default: return 0;
    }
};

auto swapTwoCards(Card &a, Card &b) -> void {
    Card temp = a;
    a = b;
    b = temp;
}

auto getRandomNumber() -> int {
   	std::random_device rd;
	std::mt19937 mersenne(rd());
	std::uniform_int_distribution<> random_card(0, 51);
	return random_card(mersenne);
}

auto shuffleCards(std::array<Card, 52> &deck_of_cards) -> std::array<Card, 52> {
    for (auto &card : deck_of_cards) {
        int random = getRandomNumber();
        swapTwoCards(card, deck_of_cards[random]);
    }
    return deck_of_cards;
}

auto dealCard(std::array<Card, 52> &deck_of_cards, int &card_order) -> Card {
        card_order++;
        return deck_of_cards[card_order];
}

auto initDeck() -> std::array<Card, 52> {
    std::array<Card, 52> deck_of_cards;
    int card = 0;
    for (int suit = 0; suit < MAX_SUITS; ++suit)
    for (int rank = 0; rank < MAX_RANKS; ++rank)
    {
        deck_of_cards[card].suit = static_cast<CardSuit>(suit);
        deck_of_cards[card].rank = static_cast<CardRank>(rank);
        ++card;
    }
    return deck_of_cards;
}

auto initDeal(Player &player1, Player &dealer, std::array<Card, 52> &shuffled_deck, int &card_order, Card &secret) -> void {
    // for player //
    Card dealt_card;

    // deal player two cards //
    std::cout << "Player's hand:\n";
    dealt_card = dealCard(shuffled_deck, card_order);
    printCard(dealt_card);
    player1.value += getCardValue(dealt_card);
    player1.card_counter++;

    dealt_card = dealCard(shuffled_deck, card_order);
    printCard(dealt_card);
    player1.value += getCardValue(dealt_card);
    player1.card_counter++;

    // deal dealer two cards //
    std::cout << "Dealer's hand:\n";
    dealt_card = dealCard(shuffled_deck, card_order);
    std::cout << "Card dealt: SECRET\n";
    dealer.value += getCardValue(dealt_card);
    secret = dealt_card;
    dealer.card_counter++;

    dealt_card = dealCard(shuffled_deck, card_order);
    printCard(dealt_card);
    dealer.value += getCardValue(dealt_card);
    dealer.card_counter++;
}

auto playerRound(Player &player1, std::array<Card, 52> &shuffled_deck, int &card_order, Card dealt_card) -> int {
    // player's move //
    std::cout << "player's turn\n";
    while (player1.value < 21) {
        std::cout << "hit (1) or stand? (0): ";
        bool hit_stand;
        std::cin >> hit_stand;
        if (hit_stand) {
            dealt_card = dealCard(shuffled_deck, card_order);
            printCard(dealt_card);
            player1.value += getCardValue(dealt_card);
            player1.card_counter++;
        }
        else
            break;
    }

    // vyhodnoceni hry //
    if (player1.value == 21) {
        std::cout << "you have reached " << player1.value << " with " << player1.card_counter << " cards!\n";
        return player1.value;
    }
    else if (player1.value > 21) {
        std::cout << "you have lost\n";
        return player1.value;
    }
    else {
        std::cout << "you have reached " << player1.value << " with " << player1.card_counter << " cards\n";
        return player1.value;
    }

}

auto dealerRound (Player &dealer, Player &player1, std::array<Card, 52> &shuffled_deck, int &card_order, Card dealt_card, Card secret) -> int  {
    if (player1.value > 21)
        return 21;
    else {
        std::cout << "dealer's turn\n";
        std::cout << "SECRET ";
        printCard(secret);
        // dealer's move //
        while (dealer.value <= 17) {
            dealt_card = dealCard(shuffled_deck, card_order);
            printCard(dealt_card);
            dealer.value += getCardValue(dealt_card);
            dealer.card_counter++;
        }

        // vyhodnoceni hry //
        if (dealer.value == 21) {
            std::cout << "dealer has reached " << dealer.value << " with " << dealer.card_counter << " cards!\n";
            return dealer.value;
        }
        else if (dealer.value > 21) {
            std::cout << "dealer has lost\n";
            return dealer.value;
        }
        else {
            std::cout << "dealer has reached " << dealer.value << " with " << dealer.card_counter << " cards\n";
            return dealer.value;
        }
    }
}

auto roundResult(Player &player1, Player &dealer) -> void {
    if (dealer.value <= 21) {
        if (player1.value > dealer.value) {
            std::cout << "you have won\n";
        }
        if (player1.value < dealer.value) {
            std::cout << "you have lost\n";
        }
        if (player1.value == dealer.value) {
            if (player1.card_counter < dealer.card_counter) {
                std::cout << "you have won, because you had had fewer cards\n";
            }
            if (player1.card_counter > dealer.card_counter) {
                std::cout << "you have lost, because you had had more cards\n";
            } else {
                std::cout << "it's a tie on the value and card quantity\n";
            }
        }
    }
    else {
        std::cout << "you have won\n";
    }

}

auto playBlackJack() -> void {
    bool play_again = true;
    Card secret;
    do {
        // initialize the game //
        std::array<Card, 52> deck_of_cards = initDeck();
        std::array<Card, 52> shuffled_deck = shuffleCards(deck_of_cards);
        Player player1 { 0, 0 };
        Player dealer { 0, 0 };
        int card_order = -1;
        Card dealt_card;
        initDeal(player1, dealer, shuffled_deck, card_order, secret);
        playerRound(player1, shuffled_deck, card_order, dealt_card);
        dealerRound(dealer, player1, shuffled_deck, card_order, dealt_card, secret);

        //checks if you haven't lost already //
        if (player1.value <=21) {
            roundResult(player1, dealer);
        }
        bool invalid_option = true;
        while(invalid_option) {
            std::cout << "would you like to play again? (y/n)";
            char yes_no;
            std::cin >> yes_no;
            if (yes_no == 'y') {
                invalid_option = false;
            }
            if (yes_no == 'n') {
                invalid_option = false;
                play_again = false;
            }
        }

    }
    while (play_again);




}

int main() {
    // menu //
    int option;
    bool menu = true;
    while (menu) {
        std::cout << "MENU:\n (1): play BlackJack\n (0): Quit\nyour option: ";
        std::cin >> option;
        switch (option) {
            case 0 : menu = false; break;
            case 1 : playBlackJack(); break;
            default: break;
        }
    }
    return 0;
}