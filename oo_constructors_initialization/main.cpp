#include <iostream>

class Something {
private:
    int m_value1;
    double m_value2;
    char m_value3;

public:
    /*Something() : m_value1(1), m_value2(2.2), m_value3('c') { // directly initialize our member variables
    // no need for assignment here
    }
    */

    Something(int value1 = 1) : m_value1(value1) {
    }

    Something(int value1 = 1) {
        m_value1 = value1;
    }

    void print() {
        std::cout << "something(" << m_value1 << ", " << m_value2 << ", " << m_value3 << ")\n";
    }
};

class Something_else
{
private:
    const int m_value;

public:
    Something_else(): m_value(5) { // directly initialize our const member variable, we can do this!
        // we wouldn't be able to assing a value to const member variable though
    }
};

class Something_else_uniform
{
private:
    const int m_value;

public:
    Something_else_uniform(): m_value { 5 } { // uniformly initialize our const member variable, we can do this! <-- THIS SHOULD BE FAVORED!
        // we wouldn't be able to assign a value to const member variable though
    }
};

int main() {
    Something something;
    something.print();
    int x {0};
    std::cout << x;
    return 0;
}


