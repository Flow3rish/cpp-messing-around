//
// Created by archvita on 8/31/18.
//

#ifndef QUIZ5_CONSTANTS_H
#define QUIZ5_CONSTANTS_H

namespace myConstants {
    const double gravity(9.8); // in meters/(second^2)
}

#endif //QUIZ5_CONSTANTS_H
