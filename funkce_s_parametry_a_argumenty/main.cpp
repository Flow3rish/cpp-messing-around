#include <iostream>

// tato funkce nebere zadne parametry
// zezavisi nijak na callerovi
void doPrint()
{
    std::cout << "In doPrint()" << std::endl;
}

// tato funkce bere jeden parametr "x"
// caller poskytne hodnotu x
void printValue(int x)
{
    std::cout << x << std::endl;
}

int add(int x, int y)
{
    return x + y;
}

int doubleNumber(int x)
{
    return 2 * x;
}



// tato funkce bere dva parametry, "x" a "y"
// caller poskytne oba tyto parametry
int main()
{
    printValue(6); //hodnote v zavorce se rika argument, je uz to konkretni hodnota, ktera se posle do funkce printValue()
    std::cout << add(2, 3) << std::endl; // 2 a 3 jsou argumenty poslane do funkce add()
    std::cout << "Vloz cislo, ktere bude zdvojnasobeno: ";
    int x;
    std::cin >> x;
    std::cout << "2 * " << x << " je " << doubleNumber(x) << std::endl; // zdvojnasobi cislo
    return 0;
}

/* parametry kazde funkce jsou pouzitelne jen uvnitr te funkce,
 * takze i kdyz printValue() a add() obe maji parametr se jmenem "x,"
 * tyto parametry jsou povazovany za oddelene a nijak se neovlivnuji */