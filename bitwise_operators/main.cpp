#include <iostream>

int main()
{
    unsigned int x = 4; // int x is 0100 in binary
    x <<= 1; // all bits in x shift to the left by 1: 0100 -> 1000. Now x evaluates to 1000 in binary (8 in decimal)
    std::cout << x << "\n";

    x = 3;
    x >>= 1; // all bits shift to the right now, there is one bit of value 1 that shifts off the end of the number,
             // bits that shift off the end of the binary number are lost forever: (3(dec) = 0011(bin)) -> (0001(bin) = 1(dec))
    std::cout << x << "\n";

    return 0;
}