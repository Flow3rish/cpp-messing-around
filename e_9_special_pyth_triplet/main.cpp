#include <iostream>


// find pythagorean triplet for which: a + b + c = 1000

int main() {
    long a, b, c;
    long searched = 0;

    for (long n = 2; n < 1000; ++n) {
        for (long m = 1; m < 1000; ++m) {
             a = (n*n) - (m*m);
             b = 2*m*n;
             c = m*m + n*n;
             if(a + b + c == 1000) {
                 std::cout << a << " + " << b << " + " << c << '\n';
                 searched = a*b*c;
                 break;
             }

        }
        if (searched > 0) {
            break;
        }
    }
    std::cout << searched << '\n';

    return 0;
}