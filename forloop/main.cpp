#include <iostream>

auto sumTo(int x) -> void {
    long long unsigned int sum{0};
    for (int i{1}; i <= x; ++i) {
        /*if (i != x) {
            std::cout << i << " + ";
        }
        else
            std::cout << i;*/
        sum += i;
    }
    //std::cout << " = " << sum << '\n';
    std::cout << sum;
}

auto forExample(){
    for (int i{0}; i <=20; i += 2) {
        std::cout << i << " ";
    }
}

int main() {
    std::cout << "Daj integera: ";
    long long unsigned int x;
    std::cin >> x;
    std::cout << '\n';

    sumTo(x);

    return 0;
}