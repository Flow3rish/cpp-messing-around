cmake_minimum_required(VERSION 3.12)
project(local_scope)

set(CMAKE_CXX_STANDARD 14)

add_executable(local_scope main.cpp)