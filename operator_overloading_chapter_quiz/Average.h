//
// Created by archvita on 9/29/18.
//

#ifndef OPERATOR_OVERLOADING_CHAPTER_QUIZ_AVERAGE_H
#define OPERATOR_OVERLOADING_CHAPTER_QUIZ_AVERAGE_H


#include <stdint-gcc.h>
#include <iostream>

class Average {
private:
    int32_t m_sum;
    int8_t m_count;
public:
    Average(){}
    Average& operator += (int);
    friend std::ostream& operator<< (std::ostream&, Average&);
};



#endif //OPERATOR_OVERLOADING_CHAPTER_QUIZ_AVERAGE_H
