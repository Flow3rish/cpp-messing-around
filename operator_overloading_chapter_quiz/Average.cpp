//
// Created by archvita on 9/29/18.
//

#include <iostream>
#include "Average.h"

Average& Average::operator+= (int num) {
    m_sum += num;
    ++m_count;
    return *this;
}

std::ostream& operator<< (std::ostream &out, Average &avg) {
    out << static_cast<double>(avg.m_sum) / avg.m_count;
    return out;
}