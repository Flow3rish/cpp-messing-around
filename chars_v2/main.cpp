#include <iostream>

int main()
{
    std::cout << "First line\nSecond line" << std::endl;    // vlozi break, pokracuje na novem radku
    std::cout << "First part\tSecond part" << std::endl;    // vlozi tab
    return 0;
}