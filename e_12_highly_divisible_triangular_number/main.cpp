#include <iostream>

int getNthTriangularNumber(int n) {
    int result = 0;
    for (int i = 0; i < n; ++i) {
        result += (i + 1);
    }
    return result;
}

int getNumberOfDivisorsOf(int number) {
    int divisor_count = 0;
    for (int i = 0; i < (number / 2) + 1; ++i) {
        if (number % (i + 1) == 0) {
            ++divisor_count;
        }
    }
    return divisor_count;
}

int main() {
    int divisor_count = 0;
    //int i = 0;
    //int t_number;
    //while (divisor_count <= 500) {
    //    divisor_count = getNumberOfDivisorsOf(getNthTriangularNumber(i + 1));
    //    t_number = getNthTriangularNumber(i + 1);
    //    std::cout << t_number << '\t' << divisor_count << " divisors.\n";
    //    ++i;
    //}
    //std::cout << t_number << " with " << divisor_count << " divisors." << '\n';

    int triangular_number = 1;
    int k = 2;
    while (divisor_count <= 500) {
        divisor_count = getNumberOfDivisorsOf(triangular_number);
        std::cout << k - 1 << ". \t" << triangular_number << " with " << divisor_count << " divisors\n";
        triangular_number += k;
        ++k;
    }



    return 0;
}