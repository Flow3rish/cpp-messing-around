#include <iostream>

int main() {

    new int; // dynamically allocate an integer (and discard the result)
    int *ptr = new int; // dynamically allocate an integer and assign the address to ptr so we can access it later
    *ptr = 7; // assign value of 7 to allocated memory


    int *ptr1 = new int (5);    // use direct initialization
    int *ptr2 = new int { 6 };  // use uniform initialization

    std::cout << "ptr1 points to the address " << ptr1 << " which stores value " << *ptr1 << '\n';

    delete ptr1;    // return the memory pointed to by ptr1 to the operating system
    delete ptr2;
    ptr1 = nullptr; // set ptr1 to be a null pointer
    ptr2 = nullptr;


    delete ptr;
    ptr = nullptr;


    // sometimes, the OS doesn't have any memory to give to the program so operator new fails //
    // this can be handled //

    int *value = new (std::nothrow) int; // ask for an integer's worth of memory // nothrow sets pointer to null if the integer allocation fails
    if (!value) {                        // handle case where new returned null
        // do error handling here
        std::cout << "Could not allocate memory";
    }

    int *ptr3;
    // if ptr3 isn't already allocated, allocate it
    if (!ptr3)
        ptr = new int;

    delete ptr3;

    return 0;
}