#include <iostream>

enum class Colors {
    COLOR_BLACK,
    COLOR_WHITE,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_BLUE,
};

namespace ifStatement {
    void printColor(Colors color) {
        if (color == Colors::COLOR_BLACK)
            std::cout << "Black";
        else if (color == Colors::COLOR_WHITE)
            std::cout << "White";
        else if (color == Colors::COLOR_RED)
            std::cout << "Red";
        else if (color == Colors::COLOR_GREEN)
            std::cout << "Green";
        else if (color == Colors::COLOR_BLUE)
            std::cout << "Blue";
        else
            std::cout << "Unknown";
    }
}

namespace switchStatement {
    void printColor(Colors color) {
        switch (color) {
            case Colors::COLOR_BLACK:
                std::cout << "Black";
                break;
            case Colors::COLOR_WHITE:
                std::cout << "White";
                break;
            case Colors::COLOR_RED:
                std::cout << "Red";
                break;
            case Colors::COLOR_GREEN:
                std::cout << "Green";
                break;
            case Colors::COLOR_BLUE:
                std::cout << "Blue";
                break;
            default:
                std::cout << "Unknown";
                break;
        }
    }

}

int main() {
    ifStatement::printColor(Colors::COLOR_RED);
    switchStatement::printColor(Colors::COLOR_GREEN);
    return 0;
}