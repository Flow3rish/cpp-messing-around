#include <iostream>
#include <cmath>
#include <string>
#include <ctime>
#include <cstdlib>

class Point2d {
private:
    double m_x;
    double m_y;

public:

    Point2d(double x = 0.0, double y = 0.0) : m_x { x }, m_y { y } {}

    void print() {
        std::cout << "Point: x: " << m_x << " y: " << m_y << '\n';
    }

    friend double distanceTo(const Point2d&, const Point2d&);

};

double distanceTo(const Point2d &point1, const Point2d &point2) {
    return sqrt( (point1.m_x-point2.m_x)*(point1.m_x-point2.m_x) + (point1.m_y-point2.m_y)*(point1.m_y-point2.m_y) );
}



class Monster {
public:
     enum Monstertype {
         MONSTER_DRAGON,
         MONSTER_GOBLIN,
         MONSTER_OGRE,
         MONSTER_ORC,
         MONSTER_SKELETON,
         MONSTER_TROLL,
         MONSTER_VAMPIRE,
         MONSTER_ZOMBIE,
         MAX_MONSTER_TYPES
    };
private:
    Monstertype m_type;
    std::string m_name;
    std::string m_roar;
    int m_hp;

public:
    Monster();

    Monster(Monstertype type, std::string name, std::string roar, int hp)
        : m_type { type }, m_name { name }, m_roar { roar }, m_hp { hp } {

    }

    std::string getTypeString() const {
        switch (m_type) {
            case MONSTER_DRAGON : { return "Dragon"; }
            case MONSTER_GOBLIN : { return "Goblin"; }
            case MONSTER_OGRE : { return "Ogre"; }
            case MONSTER_ORC : { return "Orc"; }
            case MONSTER_SKELETON : { return "Skeleton"; }
            case MONSTER_TROLL : { return "Troll"; }
            case MONSTER_VAMPIRE : { return "Vampire"; }
            case MONSTER_ZOMBIE : { return "Zombie"; }
            default : { return " "; }
        }
    }

    void print() const {
        std::cout << m_name << " the " << getTypeString() << " has " << m_hp << " hit points nad says " << m_roar << '\n';
    }


};

class MonsterGenerator {
public:

    // Generate a random number between min and max (inclusive)
	// Assumes srand() has already been called
	static int getRandomNumber(int min, int max) {
		static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);  // static used for efficiency, so we only calculate this value once
		// evenly distribute the random number across our range
		return static_cast<int>(rand() * fraction * (max - min + 1) + min);
	}

    static Monster generateMonster(){
        Monster::Monstertype type = static_cast<Monster::Monstertype>(getRandomNumber(0, Monster::MAX_MONSTER_TYPES - 1));
        int hp = getRandomNumber(1, 100);
        static std::string s_names[6]{"kokot", "potok", "totok", "pokot", "tokot", "totot"};
        static std::string s_roars[6]{"haf", "baf", "kaf", "raf", "cuck", "zucc"};

        return Monster(type, s_names[getRandomNumber(0,5)], s_roars[getRandomNumber(0, 5)], hp);
    }
};

int main() {
    Point2d first;
    Point2d second(3.0, 4.0);
    first.print();
    second.print();
    std::cout << "Distance between two points: " << distanceTo(first, second) << '\n';
    Monster skele(Monster::MONSTER_SKELETON, "Bones", "*rattle*", 4);
    skele.print();

    srand(static_cast<unsigned int>(time(0))); // set initial seed value to system clock
	rand(); // If using Visual Studio, discard first random value
    Monster m = MonsterGenerator::generateMonster();
    m.print();

    return 0;
}