#include <iostream>

auto quizFirst() -> void {
    int a{0};
    while (a <= 127) {
        std::cout << a << " " << static_cast<char>(a) << " ";
        if (a % 10 == 0)
            std::cout << '\n';
        ++a;
    }

}

auto quizSecond() -> void {
    int a{5};
    while(a >= 1) {
        int x{a};
        while (x > 0) {
            std::cout << x << " ";
            --x;
        }

        std::cout << '\n';
        --a;
    }
}

auto quizThird() -> void {
    int n{1};
    while(n <=5) {
        int x{5};
        while (x > 0) {
            if (x <= n) {
                std::cout << x << " ";
            } else
                std::cout << "  ";
            --x;
        }
        std::cout << '\n';
        n++;
    }

}


int main() {
    quizSecond();
    quizThird();

    return 0;
}