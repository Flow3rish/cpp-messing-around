#include <iostream>

/* program jede jako sekvence, takze cte od zacatku do konce,
 * pokud mu neni prikazano, aby preskocil jinam */

int addNumbers(int,int);                        // jelikoz funkce je definovana az po main(), musime ji deklarovat tzv. function prototypem

int main() {
    std::cout << addNumbers(6,8) << std::endl;
    return 0;
}

int addNumbers(int x, int y)                    // zde je definice funkce, ktera bude zavolana
{
    return x + y;
}

/* nektere definice jsou zaroven deklarace, a nektere deklarace jsou zaroven definice, neni tomu tak vzdy */