#include <iostream> // to se dava, at muzeme pouzivat std::cout a std::endl

void doNothing(const int &x)
{
}

int main() // toto je radkovy komentar
{
    std::cout << "Hello world!" << std::endl; // std::endl je neco jako enter
    int x; // toto deklaruje promennou typu integer
    x = 4; // toto vlozi hodnotu do promenne x (v nasem pripade 4)
    int y; // neinicializovana promenna

    doNothing(y); // primeli jsme compiler si myslet, ze se s promennou neco deje, takze nehodi error za neinicializovanou promennou

    std::cout << "Garbage promenna je " << y << std::endl;


    std::cout << "Enter a number: "; //ask user for a number
    int hovno; // neni treba inicializovat hovno, davat mu hodnotu, protoze to vlozi user
    std::cin >> hovno; //precte cislo z konzole a ulozi do hovno
    std::cout << "You entered " << hovno << std::endl;

    return 0;
}

/* toto
 * je
 * viceradkovy
 * komentar
 * asterisky
 * usnadnuji
 * cteni */
