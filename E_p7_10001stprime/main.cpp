#include <iostream>

bool isPrime(int n) {
    for (int i {3}; i <= n/2 + 1; ++i) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}


int main() {
    int prime_count { 2 };
    int tested_number { 4 };
    int searched_number;

    for (tested_number; prime_count <= 10001; ++tested_number) {
        if (isPrime(tested_number)){
            searched_number = tested_number;
            ++prime_count;
        }
    }
    std::cout << "count " << prime_count << '\n';
    std::cout << "prime " << searched_number << '\n';

    return 0;
}