#include <iostream>

// find the difference between the sum of the squares of the first 100 natural numbers and the square of the sum
// (1 + 2 + 3 + ... + n) - (1^2 + 2^2 + .... + n^2)

int generateSquaredSum() {
    int sum = 0;
    for (int i = 0; i < 100; ++i) {
        sum += (i + 1);
    }
    sum *= sum;
    return sum;
}

int generateSumOfSquares() {
    int sum = 0;
    for (int i = 0; i < 100; ++i) {
        sum += (i + 1)*(i + 1);
    }
    return sum;
}

int main() {
    int a = generateSquaredSum();
    int b = generateSumOfSquares();

    std::cout << a - b << '\n';
    return 0;
}