//
// Created by archvita on 9/21/18.
//

#include "operator_overloading.h"
#include <iostream>

Cents operator+(const Cents &c1, const Cents &c2) {
    return Cents(c1.m_cents + c2.m_cents);
}

Cents operator-(const Cents &c1, const Cents &c2) {
    return Cents(c1.m_cents - c2.m_cents);
}

Cents operator+(const Cents &c1, int value2) {
    return Cents(c1.m_cents + value2);
}

Cents operator+(int value1, const Cents &c2) {
    return Cents(value1 + c2.m_cents);
}

Cents operator/(const Cents &c1, const Cents &c2) {
    return Cents(c1.getCents() / c2.getCents());
}

Cents operator/(const Cents &c1, int value) {
    return Cents(c1.getCents() / value);
}

Cents operator/(int value, const Cents &c2) {
    return Cents(value / c2.getCents());
}


#include "operator_overloading.h"
