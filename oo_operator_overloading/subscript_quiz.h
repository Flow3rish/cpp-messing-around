//
// Created by archvita on 9/28/18.
//

#ifndef OO_OPERATOR_OVERLOADING_SUBSCRIPT_QUIZ_H
#define OO_OPERATOR_OVERLOADING_SUBSCRIPT_QUIZ_H

#include <string>
#include <vector>


struct StudentGrade {
    std::string name;
    char grade;
};

class GradeMap {
private:
    std::vector<StudentGrade> m_map;
public:
    GradeMap(){}
    char& operator[] (std::string);
};

#endif //OO_OPERATOR_OVERLOADING_SUBSCRIPT_QUIZ_H
