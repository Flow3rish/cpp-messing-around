//
// Created by archvita on 9/21/18.
//

#ifndef OO_OPERATOR_OVERLOADING_QUIZ_H
#define OO_OPERATOR_OVERLOADING_QUIZ_H

#include <iostream>

class Fraction {
private:
    int m_numerator;
    int m_denominator;

public:
    Fraction();
    Fraction(int, int);
    void print();
    void reduce();

    // getters //
    int getNumerator() const { return m_numerator; }
    int getDenominator() const { return m_denominator; }

    // setters //
    void setNumerator(const int numerator) { m_numerator = numerator; }
    void setDenominator(const int denominator) { m_denominator = denominator;}


    friend Fraction operator*(const Fraction&, const Fraction&);
    friend Fraction operator*(const Fraction&, int);
    friend Fraction operator*(int, const Fraction&);
    friend int gcd(int, int);

};

std::ostream& operator<< (std::ostream &out, Fraction&);
std::istream& operator>> (std::istream&, Fraction&);

#endif //OO_OPERATOR_OVERLOADING_QUIZ_H
