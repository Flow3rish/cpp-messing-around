//
// Created by archvita on 9/28/18.
//

#include "list_overloading.h"
#include <iostream>

int& IntList::operator[](const int index) {
    return m_list[index];
}

const int& IntList::operator[](const int index) const { // for const objects: can only be used for access
    return m_list[index];
}

