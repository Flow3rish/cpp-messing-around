//
// Created by archvita on 9/28/18.
//

#ifndef OO_OPERATOR_OVERLOADING_LIST_OVERLOADING_H
#define OO_OPERATOR_OVERLOADING_LIST_OVERLOADING_H

class IntList {
private:
    int m_list[10];
public:
    int& operator[] (const int);
    const int& operator[] (const int) const; // a constant function returning a constant reference. defined in case the created object is constant
};



#endif //OO_OPERATOR_OVERLOADING_LIST_OVERLOADING_H
