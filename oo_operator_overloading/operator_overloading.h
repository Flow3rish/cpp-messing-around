//
// Created by archvita on 9/21/18.
//

#ifndef OO_OPERATOR_OVERLOADING_OPERATOR_OVERLOADING_H
#define OO_OPERATOR_OVERLOADING_OPERATOR_OVERLOADING_H

class Cents {
private:
    int m_cents;

public:
    Cents(int cents) : m_cents(cents) {}
    int getCents() const { return m_cents; }

    // declaring friend operator overloading functions //
    friend Cents operator+(const Cents&, const Cents&);
    friend Cents operator-(const Cents&, const Cents&);

    // we can also define the friend function inside the class //
    // this doesn't make the function a member of the class //
    friend Cents operator*(const Cents &c1, const Cents &c2) {  // however, this is NOT recommended, as non-trivial function
                                                                // definitions are better kept in a separate .cpp file
        return Cents(c1.m_cents + c2.m_cents);
    }

    // we sometimes want our operators to work with operands that are different types //
    friend Cents operator+(const Cents&, int);
    friend Cents operator+(int, const Cents&);
    // the order od parameters matter here exempli gratima: we only define the first function. if we then were to add int + Cents in this order, we would have got an error.
};

// we can overload operators with normal functions, this should be preferred, because normal functions don't interact with private variables directly //
// we also need to explicitly provide prototype for operator/ so uses of operator/ in other files know this overload exists //
Cents operator/(const Cents&, const Cents&);
Cents operator/(const Cents&, int);
Cents operator/(int, const Cents&);

#endif //OO_OPERATOR_OVERLOADING_OPERATOR_OVERLOADING_H
