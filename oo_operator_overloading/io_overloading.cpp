//
// Created by archvita on 9/21/18.
//

#include <iostream>
#include "io_overloading.h"

Point::Point(int x, int y, int z)
    : m_x { x }, m_y { y }, m_z { z }
{}

std::ostream& operator<< (std::ostream &out, Point &point) {
    out << "Point(" << point.getX() << ", " << point.getY() << ", " << point.getZ() << ")";
    return out;
}
