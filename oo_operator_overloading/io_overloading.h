//
// Created by archvita on 9/21/18.
//

#ifndef OO_OPERATOR_OVERLOADING_IO_OVERLOADING_H
#define OO_OPERATOR_OVERLOADING_IO_OVERLOADING_H

#include <iostream>

class Point {
private:
    int m_x;
    int m_y;
    int m_z;
public:
    Point(int, int, int);

    int getX() { return m_x; }
    int getY() { return m_y; }
    int getZ() { return m_z; }
};


std::ostream& operator<< (std::ostream&, Point&);
#endif //OO_OPERATOR_OVERLOADING_IO_OVERLOADING_H
