#include <iostream>
#include "operator_overloading.h"
#include "quiz.h"
#include "io_overloading.h"
#include "list_overloading.h"

int main() {
    Cents cents1 { 8 };
    Cents cents2 { 6 };

    Cents centsSum = cents1 + cents2;
    std::cout << "The sum is " << centsSum.getCents() << " cents." << std::endl;

    Cents centsDif = cents1 - cents2;
    std::cout << "The difference is " << centsDif.getCents() << " cents." << std::endl;

    int value1 { 4 };
    int value2 { 5 };
    Cents cents3 { 9 };

    Cents centsPlusInt = cents3 - value2;
    std::cout <<  cents3.getCents() << " cents plus int is " << centsPlusInt.getCents() << " cents." << std::endl;

    Cents intPlusCents = value1 + cents3;
    std::cout << "int plus " << cents3.getCents() << " cents is " << intPlusCents.getCents() << " cents." << std::endl;

    // Quiz time //
    std::cout << "Quiz time" << '\n';
    //Fraction f1(1, 4);
    //f1.print();


    Fraction f1(2, 5);
    f1.print();

    Fraction f2(3, 8);
    f2.print();

    Fraction f3 = f1 * f2;
    f3.print();

    Fraction f4 = f1 * 2;
    f4.print();

    Fraction f5 = 2 * f2;
    f5.print();

    Fraction f6 = Fraction(1, 2) * Fraction(2, 3) * Fraction(3, 4);
    f6.print();

    Point point(3, 4, 5);
    std::cout << "You entered " << point << '\n';

    //// io overloading quiz time //
    //Fraction f7;
    //std::cout << "enter the first fraction: ";
    //std::cin >> f7;
    //std::cout << f7 << '\n';

    IntList list;
    list[2] = 3; // set a value
    std::cout << list[2] << '\n';

    // const IntList clist;
    // clist[2] = 3; // compile error: calls const version of operator[], which returns a const reference. cannot assign to this



    return 0;
}