//
// Created by archvita on 9/28/18.
//

#include "subscript_quiz.h"


char& GradeMap::operator[] (std::string name) {
    for (auto &ref : m_map) {
        if(ref.name == name) {
            return ref.grade;
        }
    }
    StudentGrade temp { name, ' ' };
    m_map.push_back(temp);
    return m_map.back().grade;
}
