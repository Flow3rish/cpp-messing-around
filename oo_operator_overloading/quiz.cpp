//
// Created by archvita on 9/21/18.
//

#include "quiz.h"
#include <iostream>

Fraction::Fraction() {
    m_numerator = 0;
    m_denominator = 0;
}

Fraction::Fraction(int numerator, int denominator) :
    m_numerator { numerator }, m_denominator { denominator } {
    this -> reduce();
}

void Fraction::print() {
    std::cout << m_numerator << "/" << m_denominator << '\n';
}

Fraction operator*(const Fraction &f1, const Fraction &f2) {
    Fraction f3 ((f1.m_numerator * f2.m_numerator), (f1.m_denominator * f2.m_denominator));
    return f3;
}

// every integer n can be written as a ratio n/1 //
Fraction operator*(const Fraction &f1, int value) {
    Fraction f3 ((f1.m_numerator * value), (f1.m_denominator));
    return f3;
}

Fraction operator*(int value, const Fraction &f2) {
    Fraction f3 ((value * f2.m_numerator), (f2.m_denominator));
    return f3;
}

int gcd(int a, int b) {
    return (b == 0) ? (a > 0 ? a : -a) : gcd(b, a % b);
}

void Fraction::reduce() {
    int divisor { gcd(this -> m_numerator, this -> m_denominator) };
    this -> m_numerator = this -> m_numerator / divisor;
    this -> m_denominator = this -> m_denominator / divisor;
}

std::ostream& operator<< (std::ostream &out, Fraction &fraction) {
    fraction.reduce();
    out << fraction.getNumerator() << '/' << fraction.getDenominator() << '\n';
    return out;
}

std::istream& operator>> (std::istream &in, Fraction &fraction) {
    std::cout << "input your fraction x/y: " << '\n';
    std::cout << "x: ";
    int numerator;
    in >> numerator;
    fraction.setNumerator(numerator);
    std::cout << "y: ";
    int denominator;
    in >> denominator;
    fraction.setDenominator(denominator);
    return in;
}