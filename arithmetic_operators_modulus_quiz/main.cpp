#include <iostream>

int getNumber()
{
    std::cout << "input an integer: ";
    int number;
    std::cin >> number;
    return number;
}

bool isEven(int number)
{
    return (number % 2) == 0;
}

int main()
{
    int number(getNumber());
    bool check(isEven(number));
    if (check == true)
        std::cout << number << " is even.\n";
    if (check == false)
        std::cout << number << " is odd.\n";
    return 0;
}