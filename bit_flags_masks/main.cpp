#include <iostream>
#include <bitset>

int main()
{
    const int option0 = 0;
    const int option1 = 1;
    const int option2 = 2;
    const int option3 = 3;
    const int option4 = 4;
    const int option5 = 5;
    const int option6 = 6;
    const int option7 = 7;

    const unsigned char option0a = 1 << 0;
    const unsigned char option1a = 1 << 1;
    const unsigned char option2a = 1 << 2;
    const unsigned char option3a = 1 << 3;
    const unsigned char option4a = 1 << 4;
    const unsigned char option5a = 1 << 5;
    const unsigned char option6a = 1 << 6;
    const unsigned char option7a = 1 << 7;

    unsigned int my_flags_a(0b00000000);
    const unsigned int lowMask(0b00001111);

    std::bitset<8> bits(0b00000000);
    bits.set(option3);
    std::cout << bits << "\n";

    std::cout <<"Enter an integer: ";
    int num;
    std::cin >> num;

    num &= lowMask; // remove the high bits to leave only the low bits
    std::cout << "The 4 low bits have value: " << num << '\n';

    my_flags_a |= option4a;
    std::cout << "4th of bit of my_flags has value: " << static_cast<bool>(my_flags_a & option4a) << "\n";
    std::cout << "5th bit of my_flags has value: " << static_cast<bool>(my_flags_a & option5a) << "\n";
    return 0;
}