#include <iostream>

int main()
{
    // count holds the current number to print
    int count = 1; // start at 1

    // Loop continually until we pass number 100
    while (count <= 100)
    {
        std::cout << count << "\t"; // print the current number

        // if count is evenly divisible by 20, print a new line
        if (count % 3 == 0)
            std::cout << "\n";

        count = count + 1; // go to next number
    } // end of while

    return 0;
} // end of main()