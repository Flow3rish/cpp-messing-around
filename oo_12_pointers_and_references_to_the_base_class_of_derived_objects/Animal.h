//
// Created by archvita on 10/22/18.
//

#ifndef OO_12_POINTERS_AND_REFERENCES_TO_THE_BASE_CLASS_OF_DERIVED_OBJECTS_ANIMAL_H
#define OO_12_POINTERS_AND_REFERENCES_TO_THE_BASE_CLASS_OF_DERIVED_OBJECTS_ANIMAL_H

#include <iostream>
#include <string>

class Animal {
protected:
    std::string m_name;
    const char* m_speak;

    // We're making this constructor protected because
    // we don't want people creating Animal objects directly,
    // but we still want derived classes to be able to use it.
    Animal(std::string name, const char* speak)
        : m_name(name), m_speak(speak)
    {
    }

public:
    std::string getName() { return m_name; }
    const char* speak() { return m_speak; }
};

class Cat: public Animal
{
public:
    Cat(std::string name)
        : Animal(name, "meow")
    {
    }

};

class Dog: public Animal
{
public:
    Dog(std::string name)
        : Animal(name, "woof")
    {
    }

};


#endif //OO_12_POINTERS_AND_REFERENCES_TO_THE_BASE_CLASS_OF_DERIVED_OBJECTS_ANIMAL_H
