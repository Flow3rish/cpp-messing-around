#include <iostream>


// find the smallest integer divisible by every integer from 1 to 20


int main() {
    int array[] {19, 18, 17, 16, 15, 14, 13, 12, 11 }; // the divisibility of the rest of the integers is implied
    int searched = 0;
    for (int i = 20; searched == 0; i += 20) {
        bool is_divisible = false;
        for (auto &element : array) {
            if (i % element == 0) {
                is_divisible = true;
            } else {
                is_divisible = false;
                break;
            }
        }
        if (is_divisible) {
            searched = i;
        }
    }
    std::cout << searched << '\n';

    return 0;
}