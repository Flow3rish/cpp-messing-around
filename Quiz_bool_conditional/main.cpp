#include <iostream>
/* program si vezme jednomistne cislo a vypise jestli to je prvocislo */

int inputNumber()               // vezme cislo
{
    int x;
    std::cout << "zadej jendociferne cislo: ";
    std::cin >> x;
    return x;
}

bool isPrime(int x)                  // zkontroluje, jeslti to je prvocislo
{
    if (x == 2)
        return true;
    if (x == 3)
        return true;
    if (x == 5)
        return true;
    if (x == 7)
        return true;
    else
        return false;
}

int main()
{
    int x(inputNumber());
    if (isPrime(x))
        std::cout << x << " je prvocislo." << std::endl;
    else
        std::cout << x << " neni prvocislo." << std::endl;


    return 0;

}