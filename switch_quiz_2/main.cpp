#include <iostream>
#include <string>

typedef std::string text;

enum class Animal {
    PIG,
    CHICKEN,
    GOAT,
    CAT,
    DOG,
    OSTRICH,
};

auto getAnimalName(Animal animal) -> text {
    switch(animal) {
        default: {
            return "Jsi pica";
        }
        case Animal::PIG: {
            return "Pig";
        }
        case Animal::CHICKEN: {
            return "Chicken";

        }
        case Animal::GOAT: {
            return "Goat";

        }
        case Animal::CAT: {
            return "Cat";

        }
        case Animal::DOG: {
            return "Dog";

        }
        case Animal::OSTRICH: {
            return "Ostrich";

        }
    }
}

auto printNumberOfLegs(Animal animal, text animal_name) -> void {
        using std::cout;
        switch(animal) {
            default: {
                break;
            }
            case Animal::PIG: {
                cout << animal_name << " has 4 legs\n";
                break;
            }
            case Animal::CHICKEN: {
                cout << animal_name << " has 2 legs\n";
                break;
            }
            case Animal::GOAT: {
                cout << animal_name << " has 4 legs\n";
                break;
            }
            case Animal::CAT: {
                cout << animal_name << " has 4 legs\n";
                break;
            }
            case Animal::DOG: {
                cout << animal_name << " has 4 legs\n";
                break;
            }
            case Animal::OSTRICH: {
                cout << animal_name << " has 2 legs\n";
                break;
            }
    }
}



auto main() -> int {
    text animal_name{getAnimalName(Animal::CAT)};
    printNumberOfLegs(Animal::CAT, animal_name);

    text animal_name_2{getAnimalName(Animal::CHICKEN)};
    printNumberOfLegs(Animal::CHICKEN, animal_name_2);


    return 0;
}