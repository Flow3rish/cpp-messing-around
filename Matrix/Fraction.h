//
// Created by archvita on 10/26/18.
//

#ifndef MATRIX_FRACTION_H
#define MATRIX_FRACTION_H

#include <iostream>

class Fraction {
private:
    int m_numerator = 0;
    int m_denominator = 1;
public:

    // getters //
    int getNumerator() const;
    int getDenominator() const;

    // setters //
    void setNumerator(int value);
    void setDenominator(int value);
    void setFraction(int numerator_value, int denominator_value);

    // constructors //
    Fraction();
    Fraction(int numerator);
    Fraction(int numerator, int denominator);

    // destructors //

    // overloaded operators //
    void operator+= (const Fraction &f);
    friend std::ostream& operator<< (std::ostream &out, const Fraction &fraction);
    friend Fraction operator* (const Fraction &f1, const Fraction &f2);

    // functions //
    void print();
    void reduce();
    static int gcd(int numerator, int denominator);
};


#endif //MATRIX_FRACTION_H
