//
// Created by archvita on 10/26/18.
//

#include "Matrix.h"
#include <iostream>
#include <cassert>
#include <fstream>

// Matrix getters //
Fraction& Matrix::getElement (int row, int column) const {
    return m_matrix[row][column];
}
int Matrix::getRows() const { return this->m_rows; }
int Matrix::getColumns() const { return this->m_columns; }


// Matrix constructors //
Matrix::Matrix()
    : m_rows(1), m_columns(1)
{
    assert(m_rows > 0 && m_columns > 0);
    //std::cout << "creating matrix...\n";
    m_matrix = new Fraction*[1];
    for (int i = 0; i < 1; ++i) {
        m_matrix[i] = new Fraction[1];
    }
    this->setZeroMatrix();
}

Matrix::Matrix(int rows, int columns)
        : m_rows(rows), m_columns(columns)
{
    assert(m_rows > 0 && m_columns > 0);
    //std::cout << "creating matrix...\n";
    m_matrix = new Fraction*[rows];
    for (int i = 0; i < rows; ++i) {
        m_matrix[i] = new Fraction[columns];
    }
    this->setZeroMatrix();
}

// Matrix copy constructors //
Matrix::Matrix(const Matrix &source)
: m_rows(source.m_rows), m_columns(source.m_columns)
{
    assert(m_rows > 0 && m_columns > 0);
    //std::cout << "copying matrix...\n";
    m_matrix = new Fraction*[source.m_rows];
    for (int i = 0; i < source.m_rows; ++i) {
        m_matrix[i] = new Fraction[source.m_columns];
    }
    this->setZeroMatrix();
}

// Matrix Row constructors //
Matrix::Row::Row(Fraction *row)
        : m_row(row) {}

// Matrix destructors //
Matrix::~Matrix() {
    delete[] m_matrix;
    //std::cout << "deleting matrix...\n";
}

// Matrix overloaded operators //
Fraction Matrix::Row::operator[] (const int index) {
    return m_row[index];
}


Matrix::Row Matrix::operator[] (const int index) {
    return Row(m_matrix[index]);
}

// Matrix Assignment operator
Matrix& Matrix::operator=(const Matrix & source) {
    // check for self-assignment
    if (this == &source)
        return *this;

    // first we need to deallocate any value that this is holding!
    delete[] m_matrix;

    // because m_rows and m_columns are not pointers, we can shallow copy it
    m_rows = source.m_rows;
    m_columns = source.m_columns;

    // m_data is a pointer, so we need to deep copy it if it is non-null
    if (source.m_matrix) {

        // allocate memory for our copy
        m_matrix = new Fraction*[m_rows];

        // do the copy
        for (int i = 0; i < m_rows; ++i) {
            m_matrix[i] = new Fraction[m_columns];
        }
        this->setZeroMatrix();
    } else
        m_matrix = nullptr;

    return *this;
}

// Matrix functions //
void Matrix::setZeroMatrix() {
    for (int i = 0; i < m_rows; ++i) {
        for (int j = 0; j < m_columns; ++j) {
            this->getElement(i, j).setFraction(0, 1);
        }
    }
}

void Matrix::print() {
    for (int i = 0; i < m_rows; ++i) {
        std::cout << "|\t";
        for (int j = 0; j < m_columns; ++j) {
            std::cout << this->getElement(i, j) << '\t';
        }
        std::cout << "|\n";
    }
}

void Matrix::printLatex(std::string file_name) {
    std::ofstream my_file;
    my_file.open (file_name);
    my_file << "\n$$\n\\begin{matrix}\n";
    for (int i = 0; i < m_rows; ++i) {
        for (int j = 0; j < m_columns; ++j) {
            my_file << this->getElement(i, j) << "\t&" << '\t';
        }
        my_file << "\\\n";
    }
    my_file << "\\end{matrix}\n$$\n";
    my_file.close();
}

void Matrix::resize(int rows, int columns) { // all values will be lost
    Matrix *temp = new Matrix(rows, columns);
    *this = *temp;
    delete temp;
}

Matrix& Matrix::multiplyBy(const Matrix &b) {
    assert(this->m_columns == b.m_rows);

    // perform the product operation //
    auto *product = new Matrix(this->m_rows, b.m_columns); // create a temp matrix to store the product
    int m = 0; //this->m_rows
    int k = 0; //b.m_columns
    while (k < b.m_columns) {
        m = 0;
        while (m < this->m_rows) {
            for (int left_column = 0; left_column < this->m_columns; ++left_column) {
                product->getElement(m, k) += this->getElement(m, left_column) * b.getElement(left_column, k);
                //std::cout << "ELEMENT(" << m << " " << k << ") = " << product->getElement(m, k) << "\n";
            }
            ++m;
        }
        ++k;
    }

    // resize the original matrix to perform the transformation //
    this->resize(this->m_rows, b.m_columns);
    std::cout << "rows " << this->m_rows << " columns: " << this->m_columns << '\n';
    // copy the into the original matrix //
    for (int k = 0; k < this->m_rows; ++k) {
        for (int i = 0; i < this->m_columns; ++i) {
            this->getElement(k, i) = product->getElement(k, i);
        }
    }
    delete product;
    return *this;
}


Matrix& Matrix::multiplyBy(const Fraction &scalar) {
    for (int i = 0; i < this->m_rows; ++i) {
        for (int j = 0; j < this->m_columns; ++j) {
            this->getElement(i, j) = this->getElement(i, j) * scalar; // nemam overloadnute *= a nechce se mi to psat xd
        }
    }
    return *this;
}

void Matrix::interchangeRows(const int first_row_index, const int second_row_index) {
    auto *temp = new Fraction[this->m_columns];
    for (int i = 0; i < this->m_columns; ++i) {
        temp[i] = this->getElement(first_row_index, i);
    }

    for (int j = 0; j < this->m_columns; ++j) {
        this->getElement(first_row_index, j) = this->getElement(second_row_index, j);
    }

    for (int k = 0; k < this->m_columns; ++k) {
        this->getElement(second_row_index, k) = temp[k];
    }

    delete[] temp;
}

Matrix& Matrix::multiplyRow(const Fraction &scalar, const int row_index) {
    for (int i = 0; i < this->m_columns; ++i) {
        this->getElement(row_index, i) = this->getElement(row_index, i) * scalar;
    }
    return *this;
}

Matrix& Matrix::addRowToRow(const Fraction &multiple, const int adding_index, const int added_to_index) {
    for (int i = 0; i < this->m_columns; ++i) {
        this->getElement(added_to_index, i) += (multiple * this->getElement(adding_index, i));
    }
    return *this;
}

Matrix& Matrix::addRowToRow(const int multiple, const int adding_index, const int added_to_index) {
    for (int i = 0; i < this->m_columns; ++i) {
        this->getElement(added_to_index, i) += (Fraction(multiple,1) * this->getElement(adding_index, i));
    }
    return *this;
}
// SquareMatrix constructors //
SquareMatrix::SquareMatrix(int dimension)
    : Matrix(dimension, dimension){

}

// SquareMatrix functions //
void SquareMatrix::setUnitMatrix() {
    this->setZeroMatrix();
    for (int i = 0; i < m_rows; ++i) {
        for (int j = 0; j < m_columns; ++j) {
            if (i == j) {
                this->getElement(i, j).setFraction(1, 1);
            }
        }
    }
}
