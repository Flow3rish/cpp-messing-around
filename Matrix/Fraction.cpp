//
// Created by archvita on 10/26/18.
//

#include "Fraction.h"
#include <cassert>
#include <iostream>

// constructors //
Fraction::Fraction()
: m_numerator(0), m_denominator(1)
{}

Fraction::Fraction(int numerator)
    : m_numerator{numerator}, m_denominator(1)
    {} // no need to reduce

Fraction::Fraction(int numerator, int denominator)
        : m_numerator(numerator), m_denominator(denominator)
{
    assert(denominator != 0);
    this->reduce();
}

// getters //
int Fraction::getNumerator() const { return m_numerator; }
int Fraction::getDenominator() const { return m_denominator; }

// setters //
void Fraction::setNumerator(int value) { this->m_numerator = value; }
void Fraction::setDenominator(int value) { this->m_denominator = value; }
void Fraction::setFraction(int numerator_value, int denominator_value) {
    assert(denominator_value != 0);
    this->setNumerator(numerator_value);
    this->setDenominator(denominator_value);
}

// overloaded operators //
std::ostream& operator<<(std::ostream &out, const Fraction &fraction)
{
	if (fraction.getDenominator() == 1) {
        out << fraction.getNumerator();
	} else {
        out << fraction.getNumerator() << "/" << fraction.getDenominator();
    }
	return out;
}

Fraction operator* (const Fraction &f1, const Fraction &f2) {
    Fraction f3;
    f3.setFraction(f1.getNumerator() * f2.getNumerator(), f1.getDenominator() * f2.getDenominator());
    f3.reduce();
    return f3;
}

void Fraction::operator+= (const Fraction &f) {
    Fraction temp;
    if (this->m_denominator != f.m_denominator) {
        temp.m_denominator = this->m_denominator * f.m_denominator;
        this->m_numerator = this->m_numerator * f.m_denominator + f.m_numerator * this->m_denominator;
        this->m_denominator = temp.m_denominator;
    } else {
        this->m_numerator += f.m_numerator;
    }
    this->reduce();
}


// functions //
void Fraction::print() {
    std::cout << this->m_numerator << "/" << this->m_denominator;
}

void Fraction::reduce() {
  		int gcd = Fraction::gcd(m_numerator, m_denominator);
		m_numerator /= gcd;
		m_denominator /= gcd;
}


int Fraction::gcd(int numerator, int denominator) {
    return denominator == 0 ? numerator : gcd(denominator, numerator % denominator);
}
