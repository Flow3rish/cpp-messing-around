//
// Created by archvita on 10/26/18.
//

#ifndef MATRIX_MATRIX_H
#define MATRIX_MATRIX_H

#include <iostream>
#include <fstream>
#include "Fraction.h"

class Matrix {
protected:
    Fraction** m_matrix;
    int m_rows;
    int m_columns;
public:
    // Matrix constructors //
    Matrix(int rows, int columns);
    class Row {
    private:
        Fraction* m_row;
    public:
        // Row constructors //
        Row(Fraction *row);
        // overloaded operators //
        Fraction operator[] (const int index);
    };

    // Matrix copy constructors //
    Matrix();

    Matrix(const Matrix &source);

    // Matrix destructors //
    ~Matrix();

    // getters //
    Fraction& getElement(int row, int column) const; // hybrid getter/setter
    int getRows() const;
    int getColumns() const;


    // overloaded operators //
    Row operator[] (const int index);
    Matrix& operator= (const Matrix & source);

    // functions //
    void setZeroMatrix(); // sets all elements to zero
    void print();
    void printLatex(std::string file_name);
    Matrix& multiplyBy(const Matrix &b); // matrix multiplication --> B*this
    Matrix& multiplyBy(const Fraction &scalar); // scalar multiplication --> n*this
    void resize(int rows, int columns); // creates a new zero matrix in place of the old matrix
    // elementary row operations //
    void interchangeRows(const int first_row_index, const int second_row_index); // swaps two rows
    Matrix& multiplyRow(const Fraction &scalar, const int row_index);
    Matrix& addRowToRow(const Fraction &multiple, const int adding, const int added_to);
    Matrix& addRowToRow(const int multiple, const int adding, const int added_to);
};

class SquareMatrix : public Matrix {
public:
    // SquareMatrix constructors //
    SquareMatrix(int dimension);

    // SquareMatrix functions //
    void setUnitMatrix();
};


#endif //MATRIX_MATRIX_H
