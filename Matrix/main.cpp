#include <iostream>
#include "Matrix.h"
#include "Fraction.h"

int main() {
    auto *matrix1 = new Matrix (10, 10);
    //auto *matrix2 = new Matrix (20, 20);

    int k = 1;
    for (int i = 0; i < matrix1->getRows(); ++i) {
        for (int j = 0; j < matrix1->getColumns(); ++j) {
            matrix1->getElement(i, j).setFraction(k, 1);
            ++k;
        }
    }

    //matrix1->print();
    //matrix1->addRowToRow(-1,0,1);
    //matrix1->print();

    matrix1->printLatex("matrixlatex.txt");

    delete matrix1;
    //delete matrix2;
    return 0;
}