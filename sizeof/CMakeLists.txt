cmake_minimum_required(VERSION 3.12)
project(sizeof)

set(CMAKE_CXX_STANDARD 14)

add_executable(sizeof main.cpp)