//
// Created by archvita on 8/27/18.
//

#include <iostream>

double takeNumberFromUser()
{
    std::cout << "Enter a number: ";
    double number;
    std::cin >> number;
    return number;
}

char takeOperatorFromUser()
{
    std::cout << "Enter an operator: ";
    char op;
    std::cin >> op;
    if (op == '+')
        return op;
    if (op == '-')
        return op;
    if (op == '*')
        return op;
    if (op == '/')
        return op;
    else
        return 0;
}

double calculateNumbers(double number1, double number2, char op)
{
    double result;
    if (op == '+')
    {
        result = number1 + number2;
        return result;
    }
    if (op == '-')
    {
        result = number1 - number2;
        return result;
    }
    if (op == '*')
    {
        result = number1 * number2;
        return result;
    }
    if (op == '/')
    {
        result = number1 / number2;
        return result;
    }
}

void writeAnswer(double number1, double number2, char op, double result)
{
    if (op != 0)
        std::cout << number1 << " " << op << " " << number2 << " = " << result << std::endl;
    else
        std::cout << "invalid operator" << std::endl;
}