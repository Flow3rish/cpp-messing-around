//
// Created by archvita on 8/27/18.
//

#ifndef QUIZ2_CALCULATIONS_H
#define QUIZ2_CALCULATIONS_H

double takeNumberFromUser();
char takeOperatorFromUser();
double calculateNumbers(double number1, double number2, char op);
void writeAnswer(double number1, double number2, char op, double result);

#endif //QUIZ2_CALCULATIONS_H
