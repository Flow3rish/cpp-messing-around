#include <iostream>
#include "calculations.h"

/* user vlozi 2 cisla a operator, program vyhodi vysledek */


int main()
{
    double x(takeNumberFromUser());
    char op(takeOperatorFromUser());
    double y(takeNumberFromUser());
    double result(calculateNumbers(x, y, op));
    writeAnswer(x, y, op, result);
    return 0;
}