// toto je zacatek header guardu. ADD_H muze byt jakekoliv unikatni jmeno. Podle dohody se pouziva jmeno header souboru
#ifndef ADD_H // header guard je vlastne podminene nadefinovani, ktere zabrani duplicitnimu include
#define ADD_H

// toto je obsah .h souboru, zde se davaji vsechny deklarace

int add(int, int); // funkcni prototyp pro add.h -- nezapomenout strednik!

// toto je konec header guardu
#endif


