#include <iostream>

void swapIntegers(int &x, int &y) {
    int temp = x;
    x = y;
    y = temp;
}

void betterSwapIntegers(int &x, int &y) {
    x = x + y;
    y = x - y;
    x = x - y;
}

int main() {
    int x = 3;
    int y = 2;
    swapIntegers(x, y);
    std::cout << "x is now " << x << " and y is now " << y << '\n';
    int a = 4;
    int b = 5;
    betterSwapIntegers(a, b);
    std::cout << "a is now " << a << " and b is now " << b << '\n';
    return 0;
}