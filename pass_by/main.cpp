#include <iostream>

auto passByValue(int value) -> void {
    value = value * 2;
}

auto passByReference(int &reference) -> void {
    reference = reference * 2;
}

auto passByAddress(int *address) -> void {
    *address = 6; // changes the value stored in address that pointer is pointing to
}


int main() {
    // pass by value demo //
    int value = 5;
    std::cout << "value before = " << value << '\n';
    passByValue(value);
    std::cout << "value after = " << value << " unchanged, since function made a copy and didn't use the ACTUAL variable.\n\n";

    // pass by reference demo //
    int refererenced_value = 5;
    std::cout << "value before = " << refererenced_value << '\n';
    passByReference(refererenced_value);
    std::cout << "value after = " << refererenced_value << " value is changed, because function used the actual variable.\n\n";

    // pass by address demo //
    int five = 5;
    int *address = &five;
    std::cout << "address points to value: " << *address << '\n';
    passByAddress(address); // okay, so this takes the address as an argument, makes copy of that address and changes the value the address is pointing to to 6
    std::cout << "address now points to value: " << five << '\n';


    // since references are implicitly dereferenced pointers and passing by address is actually passing by value (address is copied), everything is just passing by value



    return 0;
}