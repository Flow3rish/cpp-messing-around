cmake_minimum_required(VERSION 3.12)
project(Structs)

set(CMAKE_CXX_STANDARD 14)

add_executable(Structs main.cpp)