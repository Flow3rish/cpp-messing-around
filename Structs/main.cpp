#include <iostream>

/* Structs acts like a variable and hold more variables */

struct Rectangle{
    double length = 1.0; // non-static member initialization
    double width = 1.0;
};

int main() {

    Rectangle x{2.0, 2.0}; // uniform initialization

    return 0;
}

// in C++14, we can combine this initialization, though this wasn't possible in C++11 so you either couldn't
// initialize from the start and could only declare a struct, xor you could use uniform initialization