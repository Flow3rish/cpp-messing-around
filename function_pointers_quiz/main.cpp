#include <iostream>

typedef int (*arithmeticFcn)(int, int);

struct arithmeticStruct {
    char op;
    arithmeticFcn fcn;
};



int getNumberFromUser() {
    std::cout << "input an integer: ";
    int x;
    std::cin >> x;
    return x;
}

char getOperationFromUser() {
    char op;
    do {
        std::cout << "Input an operation: ";
        std::cin >> op;
    }
    while (op != '+' && op != '-' && op != '*' && op != '/');
    return op;
}

int add(int x, int y) {
    return x + y;
}

int subtract(int x, int y) {
    return x - y;
}

int multiply(int x, int y) {
    return x * y;
}

int divide(int x, int y) {
    return x / y;
}

static const arithmeticStruct arithmeticArray[4] = {
        { '+', add },
        { '-', subtract},
        { '*', multiply},
        { '/', divide}

};

arithmeticFcn getArithmeticFunction(char op) {
    for (const auto function : arithmeticArray) {
        if (op == function.op) {
            return function.fcn;
        }
    }
    return add;
}

int main() {
    int x = getNumberFromUser();
    int y = getNumberFromUser();
    char op = getOperationFromUser();
    arithmeticFcn fcn = getArithmeticFunction(op);
    std::cout << x << ' ' << op << ' ' << y << " = " << fcn(x, y) << '\n';



    return 0;
}