#include <iostream>

int add(int x, int y) // x a y jsou vytvoreny zde
{
    // x, y jsou viditelne/pouzitelne jen uvnitr teto funkce
    return x + y;
} // x, y jsou tady mimo dosah funkce a zanikaji

int main()
{
    int a = 5; // a je vytvoreno a inicializovano
    int b = 6; // b je vytvoreno a inicializovano
    // a, b jsou viditelne/pouzitelne jen uvnitr teto funkce
    std::cout << add(a, b) << std::endl;
    return 0;
} // a, b jsou tady mimo dosah funkce a zanikaji

/* "a" by se klidne v tomto pripade mohlo prejmenovat na "x" a "b" na "y,"
 * tyto promenne jsou ve sve podstate stale odlisne */