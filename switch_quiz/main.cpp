#include <iostream>
typedef int cislo;
typedef char operace;

auto calculate(cislo x, cislo y, operace o) -> cislo {
    switch (o) {
        default: {
            std::cout << "Unknown operation.";
            break;
        }
        case '+': {
            return x + y;
        }
        case '-': {
            return x - y;
        }
        case '*': {
            return x * y;
        }
        case '/': {
            return x / y;
        }
        case '%': {
            return x % y;
        }
    }

}

auto writeAnswer(cislo result) -> void {
    std::cout << "Integer answer is: " << result << '\n';
}

auto getNumber() -> cislo {
    std::cout << "Input an integer: ";
    int x;
    std::cin >> x;
    return x;
}

auto getOperator() -> operace {
    std::cout << "Input an operator: ";
    operace o;
    std::cin >> o;
    return o;
}

int main() {
    cislo cislo1{getNumber()};
    operace operace{getOperator()};
    cislo cislo2{getNumber()};
    cislo vysledek{calculate(cislo1, cislo2, operace)};
    writeAnswer(vysledek);

    return 0;
}