#include <iostream>
#include <vector>
#include <utility>

struct Student {
    std::string name;
    int grade;
};


int main() {
    std::cout << "How many students would you like to input:  ";
    int students_size;
    std::cin >> students_size;
    std::vector<Student> students_array;
    students_array.resize(students_size); // assigns desired length
    for (int i = 0; i < students_size; ++i) {
        std::cout << i + 1 << '.' << " input name: ";
        std::cin >> students_array[i].name;
        while (true) {
            std::cout << "input grade (0-100): ";
            std::cin >> students_array[i].grade;
            if (students_array[i].grade <= 100 && students_array[i].grade >= 0) {
                break;
            }
            else
                std::cout << "Input a valid grade\n";
        }
    }

    for (int i = 0; i < students_size; ++i) {
        std::cout << "Student " << students_array[i].name << " has grade " << students_array[i].grade << '\n';
    }

    std::cout << '\n';

    bool did_sort;
    for (int trim{1}; trim <= students_size - 1; ++trim) {
        // puts the largest number to the end of the array //
        for (int index{0}; index < (students_size - trim); ++index) {
            if (students_array[index].grade < students_array[index + 1].grade) {
                std::swap(students_array[index], students_array[index + 1]);
                did_sort = true;
            }
        }

        if (!did_sort) {
            std::cout << "already sorted\n";
            break;
        }
    }

    for (int i = 0; i < students_size; ++i) {
        std::cout << "Student " << students_array[i].name << " has grade " << students_array[i].grade << '\n';
    }

    return 0;
}