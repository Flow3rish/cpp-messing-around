#include <iostream>

class Something {
private:
    int m_value;
public:
    Something();
    Something(int);

    void resetValue() { m_value = 0; }

    // we must set all functions, that don't alter the variables to const in order to use them when working with const object
    int getValue() const; // note addition of const keyword here
};

Something::Something() {
    m_value = 0;
}

Something::Something(int value) {
    m_value = value;
}

int Something::getValue() const { // and here
    return m_value;
}

void printSomething(const Something &something) { // we pass by reference to improve performance
    std::cout << something.getValue() << '\n';
}

int main() {
    const Something something; // calls the default constructor to initialize the object
    const Something not_default_something { 3 };

    something.getValue();
    not_default_something.getValue();
    return 0;
}