//
// Created by archvita on 9/14/18.
//

#include <iostream>
#include "Card.h"


// constructor //

Card::Card():
m_CardRank {MAX_RANKS},
m_CardSuit {MAX_SUITS} {}

Card::Card(CardRank rank, CardSuit suit) :
    m_CardRank { rank },
    m_CardSuit { suit } {
}

Card::~Card() {
    //std::cout << "Card ";
    //Card::printCard();
    //std::cout << " destroyed\n";
}


void Card::printCard()
{
switch (m_CardRank)
    {
    	case Card::RANK_2:		std::cout << '2'; break;
    	case Card::RANK_3:		std::cout << '3'; break;
    	case Card::RANK_4:		std::cout << '4'; break;
    	case Card::RANK_5:		std::cout << '5'; break;
    	case Card::RANK_6:		std::cout << '6'; break;
    	case Card::RANK_7:		std::cout << '7'; break;
    	case Card::RANK_8:		std::cout << '8'; break;
    	case Card::RANK_9:		std::cout << '9'; break;
    	case Card::RANK_10:		std::cout << 'T'; break;
    	case Card::RANK_JACK:		std::cout << 'J'; break;
    	case Card::RANK_QUEEN:	std::cout << 'Q'; break;
    	case Card::RANK_KING:		std::cout << 'K'; break;
    	case Card::RANK_ACE:		std::cout << 'A'; break;
    }

switch (m_CardSuit)
    {
    	case Card::SUIT_CLUB:		std::cout << 'C'; break;
    	case Card::SUIT_DIAMOND:	std::cout << 'D'; break;
    	case Card::SUIT_HEART:	std::cout << 'H'; break;
    	case Card::SUIT_SPADE:	std::cout << 'S'; break;
    }
}

int Card::getValue()
{
	switch (m_CardRank)
	{
	    case Card::RANK_2:		return 2;
	    case Card::RANK_3:		return 3;
	    case Card::RANK_4:		return 4;
	    case Card::RANK_5:		return 5;
	    case Card::RANK_6:		return 6;
	    case Card::RANK_7:		return 7;
	    case Card::RANK_8:		return 8;
	    case Card::RANK_9:		return 9;
	    case Card::RANK_10:		return 10;
	    case Card::RANK_JACK:		return 10;
	    case Card::RANK_QUEEN:	return 10;
	    case Card::RANK_KING:		return 10;
	    case Card::RANK_ACE:		return 11;
	}

	return 0;
}


auto swapTwoCards(Card &a, Card &b) -> void {
    Card temp = a;
    a = b;
    b = temp;
}