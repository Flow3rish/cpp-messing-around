//
// Created by archvita on 9/14/18.
//

#ifndef QUIZ8_FINAL_DECK_H
#define QUIZ8_FINAL_DECK_H

#include "Card.h"
#include <iostream>
#include <array>
#include "player.h"

class Deck {
private:
    static const int length { 52 };
    std::array<Card, length> m_deck;
    int m_deck_index { 0 };
public:
    Deck();
    Card& getCardFromDeck() { return m_deck[m_deck_index]; }
    static int getLength() { return length; }
    int getIndex() { return m_deck_index; }
    void setIndex(int index) { m_deck_index = index; }
    void incrementIndex() { ++m_deck_index; }

    void print();
    void shuffle();
    friend void swapTwoCards(Card &, Card &);
    //friend void dealCard(Deck &);
};






#endif //QUIZ8_FINAL_DECK_H
