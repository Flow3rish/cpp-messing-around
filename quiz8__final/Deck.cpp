//
// Created by archvita on 9/14/18.
//

#include "Deck.h"
#include <iostream>
#include <array>
#include "utils.h"


// constructor //
Deck::Deck()
{
    int i{0};
	for (int suit { 0 }; suit < Card::MAX_SUITS; ++suit)
		for (int rank { 0 }; rank < Card::MAX_RANKS; ++rank) {
			Card card{static_cast<Card::CardRank>(rank), static_cast<Card::CardSuit>(suit)};
			m_deck[i] = card;
			++i;
		}
}

void Deck::print() {
    for (int i { 0 }; i < Deck::length; ++i) {
        m_deck[i].printCard();
        std::cout << ' ';
    }
    std::cout << '\n';
}

void Deck::shuffle() {
    for (auto &card : m_deck) {
        int random = getRandomNumber();
        swapTwoCards(card, m_deck[random]);
    }
}
