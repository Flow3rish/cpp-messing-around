//
// Created by archvita on 9/14/18.
//

#ifndef QUIZ8_FINAL_CARD_H
#define QUIZ8_FINAL_CARD_H

class Card {
public:

	enum CardSuit
	{
		SUIT_CLUB,
		SUIT_DIAMOND,
		SUIT_HEART,
		SUIT_SPADE,
		MAX_SUITS
	};

    enum CardRank
    {
    	RANK_2,
    	RANK_3,
    	RANK_4,
    	RANK_5,
    	RANK_6,
    	RANK_7,
    	RANK_8,
    	RANK_9,
    	RANK_10,
    	RANK_JACK,
    	RANK_QUEEN,
    	RANK_KING,
    	RANK_ACE,
    	MAX_RANKS
    };

private:
    CardRank m_CardRank;
    CardSuit m_CardSuit;
public:

    Card();
	Card(CardRank rank, CardSuit suit);
	~Card();
	void printCard();
	int getValue();
	friend void swapTwoCards(Card &, Card &);
};

#endif //QUIZ8_FINAL_CARD_H
