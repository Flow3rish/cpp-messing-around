//
// Created by archvita on 9/14/18.
//

#include "player.h"
#include "Deck.h"
#include "Card.h"

int Player::s_num_of_player { 0 };

Card Player::dealCard(Deck &deck) {
    Card card { deck.getCardFromDeck() };
    // so we can get another card //
    deck.incrementIndex();

    // keep track on the number of cards player is holding //
    this -> incrementCardCount();

    // stores card into player's hand //
    this -> m_hand.push_back(card);

    // get player score //
    int score { card.getValue() };
    this -> upScore(score);
    return card;
}

void Player::hitMe(Deck &deck) {
    std::cout << "Player " << getPlayerId() << ": ";
    this -> dealCard(deck).printCard();
    std::cout << '\n';
}

Player::Player() :
m_player_ID { s_num_of_player },
m_card_count { 0 },
m_score { 0 }
{
    ++s_num_of_player;
}

/*Player::~Player() {
    std::cout << "object player ID " << m_player_ID << " was destroyed\n";
}*/

void Player::print() {
    std::cout << "Player with ID " << m_player_ID << " holds " << m_card_count << " cards ( ";
    for (int i { 0 }; i < getHand().size(); ++i) {
        this -> m_hand[i].printCard();
        std::cout << ' ';
    }
    std::cout <<  ")" << '\n';
}


int Player::result() {
    if (getScore() == 21) {
        std::cout << "Player " << getPlayerId() << " has reached " << getScore() << " with " << getCardCount() << " cards!\n";
        return getScore();
    }
    else if (getScore() > 21) {
        std::cout << "Player " << getPlayerId() << " has reached " << getScore() << "and is out\n";
        return getScore();
    }
    else {
        std::cout << "Player " << getPlayerId() << " has reached " << getScore() << " with " << getCardCount() << " cards\n";
        return getScore();
    }
}

int Player::check() {
     if (getScore() == 21) {
        return getScore();
    }
    else if (getScore() > 21) {
        return getScore();
    }
    else {
        return getScore();
    }
}


Dealer::Dealer() : Player(){};


void Dealer::print() {
    std::cout << "Dealer holds " << getCardCount() << " cards (SECRET ";
    for (int i { 1 }; i < getHand().size(); ++i) {
        this -> getHand()[i].printCard();
        std::cout << ") \n";
    }
}

void Dealer::reveal() {
    std::cout << "Dealer holds " << getCardCount() << " cards ( ";
    for (int i { 0 }; i < getHand().size(); ++i) {
        this -> getHand()[i].printCard();
        std::cout << ") and has score: " << getScore() << '\n';
    }
}

void Dealer::hitMe(Deck &deck) {
    std::cout << "Dealer: ";
    this -> dealCard(deck).printCard();
    std::cout << '\n';
}

int Dealer::result() {
    if (getScore() == 21) {
        std::cout << "Dealer has reached " << getScore() << " with " << getCardCount() << " cards!\n";
        return getScore();
    }
    else if (getScore() > 21) {
        std::cout << "Dealer has reached " << getScore() << "and is out\n";
        return getScore();
    }
    else {
        std::cout << "Dealer has reached " << getScore() << " with " << getCardCount() << " cards\n";
        return getScore();
    }
}

void gameEnd(Player &player, Dealer &dealer) {
    if (player.getScore() <= 21 && dealer.getScore() <= 21) {
        if (player.getScore() > dealer.getScore()) {
            std::cout << "player " << player.getPlayerId() << " has won '\n";
        }
        if (player.getScore() < dealer.getScore()) {
            std::cout << "dealer has won\n";
        }
        if (player.getScore() == dealer.getScore()) {
            if (player.getCardCount() < dealer.getCardCount()) {
                std::cout << "player " << player.getPlayerId() << " has won, because he had had fewer cards\n";
            }
            else if (player.getCardCount() > dealer.getCardCount()) {
                std::cout << "dealer has won, because he had had fewer cards\n";
            } else {
                std::cout << "it's a tie on the value and card quantity\n";
            }
        }
    }
    if (player.getScore() > 21 && dealer.getScore() < 21) {
        std::cout << "dealer has won\n";
    }
    if (player.getScore() < 21 && dealer.getScore() > 21) {
        std::cout << "player " << player.getPlayerId() << " has won\n";
    }
    if (player.getScore() > 21 && dealer.getScore() > 21) {
        std::cout << "everyone is out.\n";
    }

}
