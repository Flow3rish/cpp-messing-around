#include <iostream>
#include <array>
#include <ctime> // for time()
#include <cstdlib> // for rand() and srand()
#include "Card.h"
#include "Deck.h"
#include "player.h"

int main() {
    // instantiate a deck //
    Deck deck;
    deck.print();

    // shuffle the deck //
    deck.shuffle();
    deck.print();

    // create a dealer //
    Dealer dealer;
    dealer.dealCard(deck);
    dealer.dealCard(deck);
    dealer.print();

    // create a player //
    Player player;
    player.dealCard(deck);
    player.dealCard(deck);
    player.print();

    // play the game //
    std::cout << "player's turn\n";
    while (player.getScore() < 21) {
        std::cout << "hit (1) or stand? (0): ";
        bool hit_stand;
        std::cin >> hit_stand;
        if (hit_stand) {
            player.hitMe(deck);
            player.check();
        }
        else
            break;
    }

    player.result();

    dealer.reveal();
    while (dealer.getScore() <= 17) {
        dealer.hitMe(deck);
        dealer.check();
    }
    dealer.result();

    gameEnd(player, dealer);

    return 0;
}