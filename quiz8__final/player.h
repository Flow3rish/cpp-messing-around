//
// Created by archvita on 9/14/18.
//

#ifndef QUIZ8_FINAL_PLAYER_H
#define QUIZ8_FINAL_PLAYER_H

#include "Deck.h"
#include <vector>
#include <string>

class Deck;
class Dealer;

class Player {
private:

    static int s_num_of_player;
    int m_player_ID;
    int m_card_count;
    int m_score;
    std::vector<Card> m_hand;

public:

    Player();
    //~Player();

    // getters //
    int getPlayerId() { return m_player_ID; }
    int getCardCount() { return m_card_count; }
    int getScore() { return m_score; }
    std::vector<Card> getHand() { return m_hand; }

    // setters //
    void incrementCardCount() { ++m_card_count; }
    void upScore(int score) { m_score += score; }

    // non trivial //
    Card dealCard(Deck &deck);
    void hitMe(Deck &deck);
    void print();
    int check();
    int result();
    friend void gameEnd(const Player &, const Dealer &);

};

class Dealer : public Player{
public:
    Dealer();
    void print();
    void reveal();
    void hitMe(Deck &deck);
    int result();
    friend void gameEnd(Player &, Dealer &);
};

void gameEnd(Player &player, Dealer &dealer);



#endif //QUIZ8_FINAL_PLAYER_H
