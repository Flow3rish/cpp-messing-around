//
// Created by archvita on 9/14/18.
//

#include <iostream>
#include <random>
#include "Deck.h"

auto getRandomNumber() -> int {
   	std::random_device rd;
	std::mt19937 mersenne(rd());
	std::uniform_int_distribution<> random_card(0, Deck::getLength());
	return random_card(mersenne);
}

#include "utils.h"
