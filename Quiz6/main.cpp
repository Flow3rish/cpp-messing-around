#include <iostream>

enum Item {
    HEALTH_POTION,
    TORCH,
    ARROW,
    MAX_ITEMS
};

int countTotalItems(int *item_quantity) {
    int total = 0;
    for (int i = 0; i < MAX_ITEMS; ++i) {
        total += item_quantity[i];
    }
    return total;
}



int main() {
    int item_quantity[MAX_ITEMS] = {2, 5, 10};

    std::cout << "The player has " << countTotalItems(item_quantity) << " items in total.\n";
    return 0;
}