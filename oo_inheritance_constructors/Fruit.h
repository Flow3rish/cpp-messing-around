//
// Created by archvita on 10/16/18.
//

#ifndef OO_INHERITANCE_CONSTRUCTORS_FRUIT_H
#define OO_INHERITANCE_CONSTRUCTORS_FRUIT_H

#include <string>

class Fruit {
private:
    std::string m_name;
    std::string m_color;
public:
    Fruit(std::string name, std::string color)
    : m_name(name), m_color(color)
    {

    }

    std::string getName() const { return m_name; }
    std::string getColor() const { return m_color; }
};

class Apple : public Fruit {
private:
    double m_fiber;
public:
    Apple(std::string name = "", std::string color = "",
        double fiber = 0)
    : Fruit(name, color), m_fiber(fiber)
    {

    }

    double getFiber() const { return m_fiber; }
};

class Banana : public Fruit {
public:
    Banana(std::string name = "", std::string color = "")
    : Fruit(name, color)
    {

    }
};



#endif //OO_INHERITANCE_CONSTRUCTORS_FRUIT_H
