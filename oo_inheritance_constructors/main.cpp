#include <iostream>
#include "Fruit.h"

int main() {
    const Apple a("Red delicious", "red", 4.2);
    const Banana b("Cavendish", "yellow");


    std::cout << a.getName() << " " << a.getColor() << " " << a.getFiber() << '\n';
    std::cout << b.getName() << " " << b.getColor() << '\n';
    return 0;
}