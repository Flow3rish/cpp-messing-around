#include <iostream>

int main() {
    {
        int hovno(8);

        {
            std::cout << hovno << '\n';
            int hovno(6);               // 8 is shadowed by 6 now   //shadowing should be generally avoided
            std::cout << hovno << '\n'; // prints 6
        }
        std::cout << hovno << '\n';     // nested hovno is destroyed 8 is printed again
    }

    /*std::cout << hovno; // hovno was destroyed, it won't print */


    return 0;
}